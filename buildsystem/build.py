from build_utils import get_all_dependencies, parse_config, retrieve_deps
import glob
import logging
import os
import shutil
import subprocess

logger = logging.getLogger()
build_root = os.path.abspath(os.getcwd())
deps_name = '.deps'
deps_path = os.path.join(build_root, deps_name)
build_config = parse_config()
build_versions = ['editor', 'game', 'engine']
cmake_build_types = ['Debug', 'RelWithDebInfo', 'Release']


def retrieve_build_version():
  return build_versions[0] if build_config['editor_mode'] in build_versions \
    else build_versions[1] if build_config['game_mode'] in build_versions \
    else build_versions[2]


def retrieve_cmake_build_type():
  return cmake_build_types[0] if build_config['debug_mode'] in cmake_build_types \
    else cmake_build_types[1] if build_config['rel_with_deb_mode'] in cmake_build_types \
    else cmake_build_types[2]


def get_build_folder_name(cmake_build_type=None):
  build_version = retrieve_build_version()
  if cmake_build_type is None:
    cmake_build_type = retrieve_cmake_build_type()
  return '.{}.{}.{}.{}'.format('build', 'x64', build_version, cmake_build_type)


def update_dependencies():
  retrieve_deps(deps_path, 'master')

  build_folder_name = get_build_folder_name()
  if not os.listdir(os.path.join(build_root, build_folder_name)):
    logger.info('No builds detected. Skipping copy_dlls()')
    return

  logger.info('Copying dlls into executable folder ...')
  cmake_build_type = retrieve_cmake_build_type()
  copy_dlls(cmake_build_type)
  logger.info('Copy finished.')


def create_build_folder():
  build_folder_name = get_build_folder_name()
  build_path = os.path.join(build_root, build_folder_name)
  if not os.path.exists(build_path):
    os.mkdir(build_path)

  logger.info('Generating cmake files ...')
  deps = get_all_dependencies(deps_path)
  deps_string = ' '.join(deps)
  cmake_build_type = retrieve_cmake_build_type()

  cmake_flags = [
    '-DCMAKE_BUILD_TYPE={}'.format(cmake_build_type),
    '-DENABLE_PCH=ON',
    '-DENABLE_IPO=ON',
    '-DENABLE_CPPCHECK=ON',
    '-DALL_DEPENDENCIES:STRING={}'.format(deps_string)]
  if build_config['dev_test']:
    logger.info('Development Test Environment ACTIVE')
    cmake_flags += ['-DDEV_TEST_ENVIRONMENT=ON']

  build_cmd = [
    'cmake',
    '-G',
    # Please ensure that you have visual studio 2019 build tools
    # And on a 64 bit architecture
    'Visual Studio 16 2019',
    '-A',
    'x64']
  build_cmd += cmake_flags
  build_cmd += ['../.']
  subprocess.check_call(build_cmd, cwd=build_path)
  logger.info('Generate finished.')


def perform_build():
  if build_config['retrieve_deps']:
    retrieve_deps(deps_path, 'master')

  cmake_build_type = retrieve_cmake_build_type()
  build_folder_name = get_build_folder_name()
  build_path = os.path.join(build_root, build_folder_name) 

  if not build_config['rebuild_only']:
    create_build_folder()

  logger.info('Building project ...')
  generate_cmd = [
    'cmake',
    '--build',
    '.',
    '--config',
    cmake_build_type]
  subprocess.check_call(generate_cmd, cwd=build_path)
  logger.info('Build project finished.')


def copy_dlls(cmake_build_type, build_location=None):
  dep_build_type = 'release' if cmake_build_type in [cmake_build_types[1], cmake_build_types[2]] else 'debug'

  build_folder_name = get_build_folder_name(cmake_build_type)
  build_path = os.path.join(build_root, build_folder_name)
  if build_location:
    exec_path = build_location
  else:
    exec_path = os.path.join(build_path, cmake_build_type)

  # Additional dll copying to be added here
  dep_libraries = get_all_dependencies(deps_path)
  for dep in dep_libraries:
    dll_path = os.path.join(deps_path, dep, 'lib', 'x64')
    if dep not in ['fmod']:
      dll_path = os.path.join(dll_path, dep_build_type)

    dlls = glob.iglob(os.path.join(dll_path, '*.dll'))
    for dll in dlls:
      if os.path.isfile(dll):
        logger.info('Copying {} to {} ...'.format(dll, exec_path))
        shutil.copy2(dll, exec_path)


def copy_config_files(cmake_build_type, build_location=None):
  config_folder = "default"

  build_folder_name = get_build_folder_name(cmake_build_type)
  build_path = os.path.join(build_root, build_folder_name)
  if build_location:
    exec_path = os.path.join(build_location, config_folder)
  else:
    exec_path = os.path.join(build_path, cmake_build_type, config_folder)
  if not os.path.exists(exec_path):
    os.mkdir(exec_path)

  configs = glob.iglob(os.path.join(build_root, config_folder, '*.json'))
  for config in configs:
    if os.path.isfile(config):
      logger.info('Copying {} to {} ...'.format(config, exec_path))
      shutil.copy2(config, exec_path)


def publish(build_location, local_build):
  if not os.path.exists(build_location):
    os.mkdir(build_location)

  if local_build:
    cmake_build_type = retrieve_cmake_build_type()
    build_folder_name = get_build_folder_name(cmake_build_type)
    build_path = os.path.join(build_root, build_folder_name)
    exec_path = glob.iglob(os.path.join(build_path, cmake_build_type, '*.exe'))

    local_build_location = os.path.join(build_location, 'local')
    if not os.path.exists(local_build_location):
      os.mkdir(local_build_location)

    for exe in exec_path:
      if os.path.isfile(exe):
        shutil.copy2(exe, local_build_location)

    cmake_build_type = retrieve_cmake_build_type()
    copy_dlls(cmake_build_type, local_build_location)
    copy_config_files(cmake_build_type, local_build_location)

  # FIXME: Handle for installer


if __name__ == "__main__":
  if build_config['retrieve_deps'] and build_config['update_deps']:
    logger.info('Updating Dependencies only ...')
    update_dependencies()
    logger.info('Update finished.')

  else:
    logger.info('Starting build ...')
    perform_build()
    logger.info('Build finished.')

    logger.info('Publishing build ...')
    build_location = os.path.join(build_root, 'build')
    # FIXME: For installer, change local_build=False
    publish(build_location=build_location, local_build=True)
    logger.info('Publish finished.')

    logger.info('Done.')