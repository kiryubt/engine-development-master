import argparse
import logging
import errno
import os
import shutil
import stat
import subprocess
import tempfile

logging.basicConfig(format='-- %(levelname)s: %(message)s', level='INFO')
logger = logging.getLogger()

def handle_readonly_files(path):
  for root, directories, files in os.walk(path):
    for directory in directories:
      os.chmod(os.path.join(root, directory), stat.S_IRWXU)
    for file in files:
      os.chmod(os.path.join(root, file), stat.S_IRWXU)


def get_all_dependencies(deps_path):
  excluded_folders = ['.git']
  deps = [dep for dep in os.listdir(deps_path) \
    if os.path.isdir(os.path.join(deps_path, dep)) and \
    not dep in excluded_folders]
  return deps


def retrieve_deps(deps_path, branch):
  temp_folder = tempfile.mkdtemp()

  cmd = [
    'git',
    'clone',
    '--single-branch',
    '--branch',
    branch,
    'git@bitbucket.org:ysjeed/third-party-dependencies.git',
    temp_folder]
  FNULL = open(os.devnull, 'w')
  logger.info('Retrieving dependencies from third-party-dependencies ...')
  subprocess.check_call(cmd, stdout=FNULL, stderr=FNULL)

  # Pull from dependencies may take some time.
  # Recommended to delete the dependencies folder after successful pull
  if os.path.exists(deps_path):
    shutil.rmtree(deps_path)
  libs = get_all_dependencies(temp_folder)
  for lib in libs:
    logger.info('Retrieving {} ...'.format(lib))
    shutil.move(os.path.join(temp_folder, lib), os.path.join(deps_path, lib))

  handle_readonly_files(temp_folder)
  shutil.rmtree(temp_folder, ignore_errors=True)
  logger.info('Dependencies retrieved.')


def parse_config():
  parser = argparse.ArgumentParser(description='Build Script', formatter_class=argparse.RawTextHelpFormatter)
  parser.add_argument('-ro', '--rebuild-only', action='store_true', default=False,
                      help='Rebuild the project without remaking CMake')
  parser.add_argument('-sr', '--skip-retrieve', action='store_true', default=False,
                      help='Skips retrieval of dependencies')
  parser.add_argument('-ud', '--update-dependencies', action='store_true', default=False,
                      help='Updates dependencies only.')
  parser.add_argument('-debug', '--debug-mode', action='store_true', default=False,
                      help='Compiles project in debug mode')
  parser.add_argument('-release', '--release-mode', action='store_true', default=False,
                      help='Compiles project in release mode')
  parser.add_argument('-reldeb', '--relwithdebinfo-mode', action='store_true', default=False,
                      help='Compiles project in RelWithDebInfo mode')
  parser.add_argument('-engine', '--engine-mode', action='store_true', default=False,
                      help='Compiles project in engine mode')
  parser.add_argument('-editor', '--editor-mode', action='store_true', default=False,
                      help='Compiles project in editor mode')
  parser.add_argument('-game', '--game-mode', action='store_true', default=False,
                      help='Compiles project in game mode')
  parser.add_argument('-devtest', '--dev-test', action='store_true', default=False,
                      help='Runs engine in development test environment')
  args = parser.parse_args()

  if not args.debug_mode and not args.release_mode and not args.relwithdebinfo_mode:
    logger.warning('NO BUILD MODE SELECTED. DEFAULT TO REL_WITH_DEB_INFO')
    args.relwithdebinfo_mode = True

  if not args.editor_mode and not args.game_mode and not args.engine_mode:
    logger.warning('NO VERSION MODE SELECTED. DEFAULT TO ENGINE')
    args.engine_mode = True

  build_config = {
    'debug_mode'        : ('Debug' if args.debug_mode else None),
    'editor_mode'       : ('editor' if args.editor_mode else None),
    'engine_mode'       : ('engine' if args.engine_mode else None),
    'game_mode'         : ('game' if args.game_mode else None),
    'release_mode'      : ('Release' if args.release_mode else None),
    'rel_with_deb_mode' : ('RelWithDebInfo' if args.relwithdebinfo_mode else None),
    'rebuild_only'      : args.rebuild_only,
    'retrieve_deps'     : not args.skip_retrieve,
    'update_deps'       : args.update_dependencies,
    'dev_test'          : args.dev_test}

  return build_config