#include <builder/builder.h>

int main(int argc, char* argv[])
{
  using namespace Reimagine;
  Builder::Builder builder;
  builder.Initialise();
  builder.Start();
  builder.Run();
  return 0;
}