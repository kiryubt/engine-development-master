/**
 * @file    core.h
 * @brief   Main engine Core
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#pragma once

#include <engine/common/type_traits.h>
#include <engine/system/system_interface.h>
#include <re_common/debugger/debugger_handler.h>
#include <re_common/logger/logger.h>

namespace Reimagine { namespace Engine { namespace System {

  class Core;

  namespace Internal
  {
    extern Core* ReimagineEngine;
  }

  /**
   * @brief Core of the engine
   */
  class Core final : ReCommon::Noncopyable
  {
  public:
    using SystemCollection = std::array<
      std::unique_ptr<SystemBaseInterface>,
      Common::to_underlying_type(SystemTypes::TOTAL_COUNT)>;

    Core(std::shared_ptr<ReCommon::Debugger::DebuggerHandler> debuggerHandler);

    /**
     * @brief Initialise all systems after creation
     */
    void Initialise(SystemCollection&& systems);

    /**
     * @brief Update all systems
     */
    void Update();

    /**
     * @brief Post update all systems
     */
    void PostUpdate();

    /**
     * @brief Destroy all systems
     */
    void Destroy();

    /**
     * @brief Determines if the engine is still running
     */
    bool IsRunning() const noexcept;

    /**
     * @brief Prepares to shutdown the entire engine
     * @pre Only called after Init and before Shutdown
     */
    void Shutdown() noexcept;

    /**
     * @brief Retrieves the System
     *        Interally used for Ipc to retrieve actual system
     * @tparam TypeE SystemTypes Enum
     */
    template <SystemTypes TypeE>
    System::SystemBaseInterface* GetSystem() const noexcept
    {
      if constexpr (SystemTypes::InputSystem >= TypeE && TypeE < SystemTypes::TOTAL_COUNT)
        return Systems[Common::to_underlying_type(TypeE)].get();
      else
        return nullptr;
    }

  private:

    ///{
    //@ Rtti functions for IPC to handle
    static float RttiGetDeltaTime();
    static float RttiGetFPS();
    static bool RttiIsFrameRateCapped();
    static uint8_t RttiGetFramesLimit();
    static void RttiSetFPSLimit(bool toLimit, uint8_t framesLimit);
    ///}

    SystemCollection Systems;
    std::shared_ptr<ReCommon::Debugger::DebuggerHandler> DebuggerHandler;
    std::atomic<bool> Active;
  };

} } }