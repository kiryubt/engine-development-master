/**
 * @file    core.cpp
 * @brief   Main Engine Core
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#include <engine/system/core.h>

namespace Reimagine { namespace Engine { namespace System {

  namespace Internal
  {
    Core* ReimagineEngine = nullptr;
  }

  Core::Core(std::shared_ptr<ReCommon::Debugger::DebuggerHandler> debuggerHandler)
    : Active{true}
    , DebuggerHandler{debuggerHandler}
  {
  }

  void Core::Initialise(SystemCollection&& systems)
  {
    LOG(INFO) << "Initialising systems.";

    Ipc::GetDeltaTime = RttiGetDeltaTime;
    Ipc::GetFPS = RttiGetFPS;
    Ipc::IsFrameRateCapped = RttiIsFrameRateCapped;
    Ipc::GetFramesLimit = RttiGetFramesLimit;
    Ipc::SetFPSLimit = RttiSetFPSLimit;

    ReCommon::Debugger::RecordStart();
    Systems = std::move(systems);

    size_t size = Systems.size();
    for (size_t i = 0; i != size; ++i)
    {
      auto& system = Systems[i];
      if (!system)
      {
        LOG(WARN) << ToString(static_cast<SystemTypes>(i)) << " could not be initialized. System not found";
        continue;
      }
      system->InitialiseInterface();
    }
    const auto& time = ReCommon::Debugger::RecordEnd();
    if (time)
      LOG(DEBUG) << "Time taken for engine to initialise: " << time->count() << "ms.";

    LOG(INFO) << "Initialised systems.";
  }

  void Core::Update()
  {
    //@TODO: Game Logic here
    for (auto& system : Systems)
    {
      if (!system)
        continue;
      system->UpdateInterface();
    }
  }
  
  void Core::PostUpdate()
  {
    // @TODO: Game logic here
    for (auto& system : Systems)
    {
      if (!system)
        continue;
      system->PostUpdateInterface();
    }
  }

  void Core::Destroy()
  {
    LOG(INFO) << "Destroying systems.";
    for (auto& system : Systems)
    {
      if (!system)
        continue;
      system->DestroyInterface();
    }
    LOG(INFO) << "Destroyed systems.";
  }

  bool Core::IsRunning() const noexcept
  {
    return Active;
  }

  void Core::Shutdown() noexcept
  {
    Active = false;
  }

  float Core::RttiGetDeltaTime()
  {
    using Internal::ReimagineEngine;
    assert(ReimagineEngine);
    const auto& frameRateHandler = ReimagineEngine->DebuggerHandler->GetFrameRateHandler();
    return frameRateHandler.GetDeltaTime();
  }

  float Core::RttiGetFPS()
  {
    using Internal::ReimagineEngine;
    assert(ReimagineEngine);
    const auto& frameRateHandler = ReimagineEngine->DebuggerHandler->GetFrameRateHandler();
    return frameRateHandler.GetFPS();
  }

  bool Core::RttiIsFrameRateCapped()
  {
    using Internal::ReimagineEngine;
    assert(ReimagineEngine);
    const auto& frameRateHandler = ReimagineEngine->DebuggerHandler->GetFrameRateHandler();
    return frameRateHandler.IsFrameRateCapped();
  }

  uint8_t Core::RttiGetFramesLimit()
  {
    using Internal::ReimagineEngine;
    assert(ReimagineEngine);
    const auto& frameRateHandler = ReimagineEngine->DebuggerHandler->GetFrameRateHandler();
    return frameRateHandler.GetFramesLimit();
  }

  void Core::RttiSetFPSLimit(bool toLimit, uint8_t framesLimit)
  {
    using Internal::ReimagineEngine;
    assert(ReimagineEngine);
    ReimagineEngine->DebuggerHandler->SetFPSLimit(toLimit, framesLimit);
  }

} } }