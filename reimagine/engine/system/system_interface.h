/**
 * @file    system_interface.h
 * @brief   interface for all system components
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#pragma once

#include <cassert>

namespace Reimagine { namespace Engine { namespace System {

  class Core;

  /**
   * @brief System Types
   */
  enum class SystemTypes : uint8_t
  {
    InputSystem = 0,    // Change code in Core GetSystem if the priority change here
    ScriptSystem,
    PhysicsSystem,
    UISystem,
    AudioSystem,
    AnimationSystem,
    AISystem,
    GraphicsSystem,
    GameObjectmanagerSystem,
    WindowSystem,
#ifdef DEV_TEST_ACTIVE
    DevelopmentTestingSystem,
#endif
    TOTAL_COUNT
  };

  /**
   * @brief Converting SystemType enum to string
   * @param systemType SystemType
   */
  static std::string ToString(SystemTypes systemType)
  {
    switch(systemType)
    {
    case SystemTypes::AISystem:
      return "AI System";
    case SystemTypes::AnimationSystem:
      return "Animation System";
    case SystemTypes::AudioSystem:
      return "Audio System";
    case SystemTypes::GameObjectmanagerSystem:
      return "Game Object Manager System";
    case SystemTypes::GraphicsSystem:
      return "Graphics System";
    case SystemTypes::InputSystem:
      return "Input System";
    case SystemTypes::PhysicsSystem:
      return "Physics System";
    case SystemTypes::ScriptSystem:
      return "Script System";
    case SystemTypes::UISystem:
      return "UI System";
    case SystemTypes::WindowSystem:
      return "Window System";
#ifdef DEV_TEST_ACTIVE
    case SystemTypes::DevelopmentTestingSystem:
      return "Development Testing System";
#endif
    default:
      // unknown
      break;
    }

    return "Unknown";
  }

  /**
   * @brief System base interface to be used with SystemInterface
   */
  class SystemBaseInterface : ReCommon::Noncopyable
  {
  public:
    /**
     * @brief Initialising the system component 
     */
    virtual void InitialiseInterface() = 0;

    /**
     * @brief Updating the system component
     */
    virtual void UpdateInterface() = 0;

    /**
     * @brief Calls after Post Updating for any logic handling (if necessary)
     */
    virtual void PostUpdateInterface() = 0;

    /**
     * @brief Destructs the system component
     */
    virtual void DestroyInterface() = 0;

    /**
     * @brief Returns the system type
     * @return system type
     */
    virtual SystemTypes GetSystemType() const noexcept = 0;
  };

  /**
   * @brief Interface for system component.
   *        System Interface should be using CRTP to allow for
   *        flexibility in debugging
   * @tparam SystemT System Type
   */
  template <typename SystemT>
  class SystemBase : public SystemBaseInterface
  {
  public:
    SystemBase(SystemTypes type, Core* engine)
      : Type{type}
      , Engine{engine}
    {
      ASSERT(Engine, "nullptr is passed into Engine!");
    }

    /**
     * @brief Initialising the system component 
     */
    void InitialiseInterface() override final
    {
      static_cast<SystemT*>(this)->Initialise();
    }

    /**
     * @brief Updating the system component
     */
    void UpdateInterface() override final
    {
      ReCommon::Debugger::RecordStart(ToString(Type));
      static_cast<SystemT*>(this)->Update();
    }

    /**
     * @brief Calls after Post Updating for any logic handling (if necessary)
     */
    void PostUpdateInterface() override final
    { 
      static_cast<SystemT*>(this)->PostUpdate();
      ReCommon::Debugger::RecordEnd(ToString(Type));
    }

    /**
     * @brief Destructs the system component
     */
    void DestroyInterface() override final
    {
      static_cast<SystemT*>(this)->Destroy();
    }

    /**
     * @brief Returns the system type
     * @return system type
     */
    SystemTypes GetSystemType() const noexcept override final
    {
      return Type;
    }


  protected:
    using SystemBaseT = SystemBase<SystemT>;
    const SystemTypes Type;
    Core* Engine;

  private:
    friend /*class*/ SystemT;
  };

} } }