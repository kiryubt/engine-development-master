#pragma once

#include <engine/common/file_system/file_handler.h>
#include <engine/input/input_manager.h>
#include <engine/system/core.h>
#include <engine/window/glfw_window.h>
#include <re_common/async/executor.h>
#include <re_common/debugger/debugger_handler.h>
#include <json/json.h>
#ifdef DEV_TEST_ACTIVE
#include <engine/dev_test/dev_test_wrapper.h>
#endif

namespace Reimagine { namespace Engine {

  class EngineHandler final : ReCommon::Noncopyable
  {
  public:

    EngineHandler(
      const std::filesystem::path& configFilePath,
      std::shared_ptr<ReCommon::Debugger::DebuggerHandler> debuggerHandler);

    /**
     * @brief Start the engine
     */
    void Start();

    /**
     * @brief Initialise the engine
     */
    void Initialise();

    /**
     * @brief Determines if the engine is running
     */
    bool IsRunning() const;

    /**
     * @brief Update of the engine
     */
    void Update();

    /**
     * @brief Post update of the engine
     */
    void PostUpdate();

    /**
     * @brief Destroy the engine
     */
    void Destroy();

    /**
     * @brief Get the instance of the engine
     */
    std::unique_ptr<System::Core> GetEngine() noexcept;

  private:
    /**
     * @brief Create system components
     */
    void CreateSystems();

    /**
     * @brief Read the json configuration
     */
    std::optional<Json::Value> JsonFileReader() const;

    /// Helper functions for CreateSystems
    std::unique_ptr<Window::WindowSystem> CreateWindowSystem();
    std::unique_ptr<Input::InputManager> CreateInputManager();
#ifdef DEV_TEST_ACTIVE
    std::unique_ptr<DevTest::DevelopmentTesting> CreateTestingEnvironment();
#endif

    std::shared_ptr<ReCommon::Debugger::DebuggerHandler> DebuggerHandler;
    std::unique_ptr<System::Core> Engine;
    std::filesystem::path ConfigFilePath;
  };

} }