/**
 * @file    ipc.h
 * @brief   Inter-Process Communication Between System Components
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#pragma once

#include <functional>

namespace Reimagine::Engine::Window { class WindowSystem; }
namespace Reimagine::Engine::Input { class InputManager; }

namespace Reimagine { namespace Engine { namespace Ipc {

  namespace Internal
  {
    using GetDeltaTimeFunctor = std::function<float()>;
    using GetFPSFunctor = std::function<float()>;
    using IsFrameCappedFunctor = std::function<bool()>;
    using GetFramesLimitFunctor = std::function<uint8_t()>;
    using SetFPSLimitFunctor = std::function<void(bool /*toLimit*/, uint8_t /*framesLimit*/)>;
  }

  /**
   * @brief Get the System Window
   */
  Window::WindowSystem* GetWindow() noexcept;

  /**
   * @brief Get the Input Manager
   */
  Input::InputManager* GetInputManager() noexcept;

  /**
   * @brief Function pointer to getting delta time
   */
  extern Internal::GetDeltaTimeFunctor GetDeltaTime;

  /**
   * @brief Function pointer to getting FPS
   */
  extern Internal::GetFPSFunctor GetFPS;

  /**
   * @brief Function pointer to getting IsFrameRateCapped
   */
  extern Internal::IsFrameCappedFunctor IsFrameRateCapped;

  /**
   * @brief Function pointer to getting frames limit
   */
  extern Internal::GetFramesLimitFunctor GetFramesLimit;

  /**
   * @brief Function pointer to limit frame rate
   */
  extern Internal::SetFPSLimitFunctor SetFPSLimit;

} } }