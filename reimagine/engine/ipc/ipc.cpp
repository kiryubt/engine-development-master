/**
 * @file    ipc.cpp
 * @brief   Inter-Process Communication Between System Components
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#include <engine/input/input_manager.h>
#include <engine/ipc/ipc.h>
#include <engine/window/glfw_window.h>

namespace Reimagine { namespace Engine { namespace Ipc {

  Window::WindowSystem* GetWindow() noexcept
  {
    using SystemTypes = System::SystemTypes;
    auto window = reinterpret_cast<Window::WindowSystem*>(
      System::Internal::ReimagineEngine->GetSystem<SystemTypes::WindowSystem>());
    if (!window)
      return nullptr;
    return window;
  }

  Input::InputManager* GetInputManager() noexcept
  {
    using SystemTypes = System::SystemTypes;
    auto inputManager = reinterpret_cast<Input::InputManager*>(
      System::Internal::ReimagineEngine->GetSystem<SystemTypes::InputSystem>());
    if (!inputManager)
      return nullptr;
    return inputManager;
  }

  ///{
  //@ Initialising extern variables
  Internal::GetDeltaTimeFunctor GetDeltaTime = nullptr;
  Internal::GetFPSFunctor GetFPS = nullptr;
  Internal::IsFrameCappedFunctor IsFrameRateCapped = nullptr;
  Internal::GetFramesLimitFunctor GetFramesLimit = nullptr;
  Internal::SetFPSLimitFunctor SetFPSLimit = nullptr;
  ///}

} } }