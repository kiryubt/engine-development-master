
#pragma once

#include <engine/input/controller/controller_manager_interface.h>

namespace Reimagine { namespace Engine { namespace Input { namespace Controller {

  class ControllerManager final : public ControllerManagerInterface
  {
  public:
    ControllerManager();

    /**
     * @brief Finds the first connected controller. If there's none, INVALID is returned
     */
    GamepadDevice IsAnyConnected() const noexcept override;

    /**
     * @brief Gets the controller
     * @param deviceNumber Gamepad Device number
     */
    ControllerInterface& GetController(GamepadDevice deviceNumber) noexcept override;

    /**
     * @brief Gets the controller
     * @param deviceNumber Gamepad Device number
     */
    const ControllerInterface& GetController(GamepadDevice deviceNumber) const noexcept override;

    /**
     * @brief Update all connected controllers
     */
    void Update() noexcept override;

    /**
     * @brief Reset all controller states
     */
    void Reset() noexcept override;

  private:
    std::array<std::unique_ptr<ControllerInterface>, Common::to_underlying_type(GamepadDevice::COUNT)> Controllers;
  };

} } } }