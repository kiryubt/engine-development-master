/**
 * @file    controller.h
 * @brief   gamepad controller
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#pragma once

#include <engine/input/controller/controller_interface.h>

namespace Reimagine { namespace Engine { namespace Input { namespace Controller {

  class Controller final : public ControllerInterface
  {
  public:
    Controller(GamepadDevice deviceNumber);

    ~Controller();

    /**
     * @brief Reset the Controller's state
     */
    void ResetState() noexcept override;

    /**
     * @brief Update every controller plugged in
     */
    void Update() noexcept override;

    /**
     * @brief Determines if Controller is connected
     */
    bool IsConnected() const noexcept override;

    /**
     * @brief Determine if Controller is first plugged in
     */
    bool PluggedIn() noexcept override;

    /**
     * @brief Determine if Controller is first plugged out
     * @param index Gamepad Device index
     */
    bool PluggedOut() noexcept override;

    /**
     * @brief Determine if a Controller button is pressed
     * @param button Gamepad Button
     */
    bool ButtonPressed(Gamepad::Button button) const noexcept override;

    /**
     * @brief Determine if a Controller button is released
     * @param button Gamepad Button
     */
    bool ButtonReleased(Gamepad::Button button) const noexcept override;

    /**
     * @brief Determine if a Controller button is on hold
     * @param button Gamepad Button
     */
    bool ButtonOnHold(Gamepad::Button button) const noexcept override;

    /**
     * @brief Retrieves Controller trigger value in raw form
     * @param trigger Gamepad Trigger
     */
    int TriggerValueRaw(Gamepad::Trigger trigger) const noexcept override;

    /**
     * @brief Retrieves Controller trigger value in normalized form
     * @param trigger Gamepad Trigger
     */
    float TriggerValueNormalized(Gamepad::Trigger trigger) const noexcept override;

    /**
     * @brief Determine if a Controller trigger is pressed
     * @param trigger Gamepad Trigger
     */
    bool TriggerPressed(Gamepad::Trigger trigger) const noexcept override;

    /**
     * @brief Determine if a Controller trigger is released
     * @param trigger Gamepad Trigger
     */
    bool TriggerReleased(Gamepad::Trigger trigger) const noexcept override;

    /**
     * @brief Determine if a Controller trigger is on hold
     * @param trigger Gamepad Trigger
     */
    bool TriggerOnHold(Gamepad::Trigger trigger) const noexcept override;

    /**
     * @brief Query the position of an analog stick as raw values
              The values retrieved by this function represent the magnitude of the analog stick in each direction.
              Note that it shouldn't be possible to get full magnitude in one direction
              unless the other direction has a magnitude of zero, as the stick has a circular movement range.
              Value returns [-32768, 32767]
     * @param stick Gamepad Stick
     * @param x [output] x-value
     * @param y [output] y-value
     */
    void GetStickXYCurrentValues(Gamepad::Stick stick, int& x, int& y) const noexcept override;

    /**
     * @brief Query the position of an analog stick as normalized values.
              The values retrieved by this function represented the magntiude of the analog stick in each direction.
              Note that it shouldn't be possible to get full magnitude in one direction
              unless the other direction has a magnitude of zero, as the stick has a circular movement range
     * @param x [output] x-value
     * @param y [output] y-value
     */
    void GetStickNormalizedXY(Gamepad::Stick stick, float& x, float& y) const noexcept override;

    /**
     * @brief Query the position of an analog stick as raw values (previous values)
              The values retrieved by this function represent the magnitude of the analog stick in each direction.
              Note that it shouldn't be possible to get full magnitude in one direction
              unless the other direction has a magnitude of zero, as the stick has a circular movement range.
              Value returns [-32768, 32767]
     * @param stick Gamepad Stick
     * @param x [output] x-value
     * @param y [output] y-value
     */
    void GetStickXYPreviousValues(Gamepad::Stick stick, int& x, int& y) const noexcept override;

    /**
     * @brief Query the magnitude of an analog stick.
              This returns the normalized value of the magnitude of the stick.
              That is, if the stick is paused all the way in any direction, it returns 1.0
     * @param stick Gamepad Stick
     */
    float GetStickMagnitude(Gamepad::Stick stick) const noexcept override;

    /**
     * @brief Get the direction the stick is pushed in (if any)
              This is a useful utility function for when the stick should be treated as a simple directional pad, such as for menu UIs
     * @param stick Gamepad Stick
     */
    Gamepad::StickDirection GetStickDirection(Gamepad::Stick stick) const noexcept override;

    /**
     * @brief Query the direction of a stick (in radians)
              This returns the direction of the stick. This value is in radians, not degrees.
              Zero is the right, and the angle increases in a counter-clockwise direction.
     * @param stick Gamepad Stick
     */
    float GetStickAngle(Gamepad::Stick stick) const noexcept override;

    /**
     * @brief Test whether a stick has been pressed in a particular direction since the last update.
              This only returns true if the stick was centered last frame.
              This is a useful utility function for when the stick should be treated as a simple directional pad, such as for menu UIs.
     * @param stick Gamepad Stick
     */
    bool GetStickDirectionTriggered(
      Gamepad::Stick stick,
      Gamepad::StickDirection direction) const noexcept override;

    /**
     * @brief Set the rumble motors on/off.
              To turn off the rumble effect, set the values to 0 for both motors.
              The left-motor is the low-frequency / strong motor, and the right motor is the high-frequency / weak motor
              Input values to be between 0 - 1
     * @param leftMotor Gamepad Left Motor
     * @param rightMotor Gamepad Right Motor
     */
    void SetRumble(float leftMotor, float rightMotor) noexcept override;

    /**
     * @brief Get rumble values on both motors
     * @param leftValue [output] left motor value
     * @param rightValue [output] right motor value
     */
    void GetRumble(float& leftValue, float& rightValue) const noexcept override;

    /**
     * @brief Get Controller device number
     */
    GamepadDevice GetDeviceNumber() const noexcept;

  private:

    /**
     * @brief Update the Controller's device
     */
    void UpdateDevice() noexcept;

    /**
     * @brief Update the Controller's thumbstick
     * @param axis Gamepad Axis
     * @param deadzone Deadzone of the thumbstick
     */
    void UpdateStick(GamepadAxis& axis, float deadzone) noexcept;

    /**
     * @brief Update the Controller's trigger
     * @param trigger Gamepad Trigger
     */
    void UpdateTrigger(GamepadTriggerInfo& triggerInfo) noexcept;

    GamepadState State;
    GamepadDevice DeviceNumber;
  };

} } } }