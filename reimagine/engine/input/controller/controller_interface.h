
#pragma once

#include <engine/input/input_common.h>
#include <engine/common/type_traits.h>
#include <re_common/noncopyable.h>
#include <glm/vec2.hpp>
#include <array>
#include "xinput.h"
#pragma comment(lib, "xinput.lib")

namespace Reimagine { namespace Engine { namespace Input {

  namespace
  {
    // Deadzones for Controller (Default for Xbox)
    static constexpr uint32_t GamepadDeadzoneLeftStickValue = 7849;
    static constexpr uint32_t GamepadDeadzoneRightStickValue = 8689;
    static constexpr uint32_t GamepadDeadzoneTriggerValue = 30;

    struct GamepadAxis
    {
      int16_t LastX = 0;
      int16_t LastY = 0;
      int16_t X = 0;
      int16_t Y = 0;
      float NormalizedX = 0;
      float NormalizedY = 0;
      float Magnitude = 0;
      float Angle = 0;
      Gamepad::StickDirection LastDirection = Gamepad::StickDirection::CENTER;
      Gamepad::StickDirection CurrentDirection = Gamepad::StickDirection::CENTER;
    };

    struct GamepadTriggerInfo
    {
      int32_t Value = false;
      float NormalizedValue = false;
      float LastNormalizedValue = false;
      bool LastPressed = false;
      bool CurrentPressed = false;
      bool LastOngoing = false;
      bool Ongoing = false;
    };

    struct GamepadState
    {
      uint8_t Flags = 0;
      uint8_t PreviousFlags = 0;
      uint32_t CurrentButtons = 0;
      uint32_t LastButtons = 0;
      bool PluggedIn = false;
      uint16_t LeftMotorValue = 0;
      uint16_t RightMotorValue = 0;
      std::array<GamepadAxis, Common::to_underlying_type(Gamepad::Stick::STICKCOUNT)> Sticks;
      std::array<GamepadTriggerInfo, Common::to_underlying_type(Gamepad::Trigger::TRIGGERCOUNT)> Triggers; 
    };
  }

  enum class GamepadDevice : int
  {
    GAMEPAD0 = 0,
    GAMEPAD1,
    GAMEPAD2,
    GAMEPAD3,
    COUNT,
    INVALID = 0xCC
  };

  class ControllerInterface : ReCommon::Noncopyable
  {
  public:
    virtual ~ControllerInterface() = default;

    /**
     * @brief Reset the Controller's state
     */
    virtual void ResetState() noexcept = 0;

    /**
     * @brief Update every controller plugged in
     */
    virtual void Update() noexcept = 0;

    /**
     * @brief Determines if Controller is connected
     */
    virtual bool IsConnected() const noexcept = 0;

    /**
     * @brief Determine if Controller is first plugged in
     */
    virtual bool PluggedIn() noexcept = 0;

    /**
     * @brief Determine if Controller is first plugged out
     * @param index Gamepad Device index
     */
    virtual bool PluggedOut() noexcept = 0;

    /**
     * @brief Determine if a Controller button is pressed
     * @param button Gamepad Button
     */
    virtual bool ButtonPressed(Gamepad::Button button) const noexcept = 0;

    /**
     * @brief Determine if a Controller button is released
     * @param button Gamepad Button
     */
    virtual bool ButtonReleased(Gamepad::Button button) const noexcept = 0;

    /**
     * @brief Determine if a Controller button is on hold
     * @param button Gamepad Button
     */
    virtual bool ButtonOnHold(Gamepad::Button button) const noexcept = 0;

    /**
     * @brief Retrieves Controller trigger value in raw form
     * @param trigger Gamepad Trigger
     */
    virtual int TriggerValueRaw(Gamepad::Trigger trigger) const noexcept = 0;

    /**
     * @brief Retrieves Controller trigger value in normalized form
     * @param trigger Gamepad Trigger
     */
    virtual float TriggerValueNormalized(Gamepad::Trigger trigger) const noexcept = 0;

    /**
     * @brief Determine if a Controller trigger is pressed
     * @param trigger Gamepad Trigger
     */
    virtual bool TriggerPressed(Gamepad::Trigger trigger) const noexcept = 0;

    /**
     * @brief Determine if a Controller trigger is released
     * @param trigger Gamepad Trigger
     */
    virtual bool TriggerReleased(Gamepad::Trigger trigger) const noexcept = 0;

    /**
     * @brief Determine if a Controller trigger is on hold
     * @param trigger Gamepad Trigger
     */
    virtual bool TriggerOnHold(Gamepad::Trigger trigger) const noexcept = 0;

    /**
     * @brief Query the position of an analog stick as raw values
              The values retrieved by this function represent the magnitude of the analog stick in each direction.
              Note that it shouldn't be possible to get full magnitude in one direction
              unless the other direction has a magnitude of zero, as the stick has a circular movement range.
              Value returns [-32768, 32767]
     * @param stick Gamepad Stick
     * @param x [output] x-value
     * @param y [output] y-value
     */
    virtual void GetStickXYCurrentValues(Gamepad::Stick stick, int& x, int& y) const noexcept = 0;

    /**
     * @brief Query the position of an analog stick as normalized values.
              The values retrieved by this function represented the magntiude of the analog stick in each direction.
              Note that it shouldn't be possible to get full magnitude in one direction
              unless the other direction has a magnitude of zero, as the stick has a circular movement range
     * @param x [output] x-value
     * @param y [output] y-value
     */
    virtual void GetStickNormalizedXY(Gamepad::Stick stick, float& x, float& y) const noexcept = 0;

    /**
     * @brief Query the position of an analog stick as raw values (previous values)
              The values retrieved by this function represent the magnitude of the analog stick in each direction.
              Note that it shouldn't be possible to get full magnitude in one direction
              unless the other direction has a magnitude of zero, as the stick has a circular movement range.
              Value returns [-32768, 32767]
     * @param stick Gamepad Stick
     * @param x [output] x-value
     * @param y [output] y-value
     */
    virtual void GetStickXYPreviousValues(Gamepad::Stick stick, int& x, int& y) const noexcept = 0;

    /**
     * @brief Query the magnitude of an analog stick.
              This returns the normalized value of the magnitude of the stick.
              That is, if the stick is paused all the way in any direction, it returns 1.0
     * @param stick Gamepad Stick
     */
    virtual float GetStickMagnitude(Gamepad::Stick stick) const noexcept = 0;

    /**
     * @brief Get the direction the stick is pushed in (if any)
              This is a useful utility function for when the stick should be treated as a simple directional pad, such as for menu UIs
     * @param stick Gamepad Stick
     */
    virtual Gamepad::StickDirection GetStickDirection(Gamepad::Stick stick) const noexcept = 0;

    /**
     * @brief Query the direction of a stick (in radians)
              This returns the direction of the stick. This value is in radians, not degrees.
              Zero is the right, and the angle increases in a counter-clockwise direction.
     * @param stick Gamepad Stick
     */
    virtual float GetStickAngle(Gamepad::Stick stick) const noexcept = 0;

    /**
     * @brief Test whether a stick has been pressed in a particular direction since the last update.
              This only returns true if the stick was centered last frame.
              This is a useful utility function for when the stick should be treated as a simple directional pad, such as for menu UIs.
     * @param stick Gamepad Stick
     */
    virtual bool GetStickDirectionTriggered(
      Gamepad::Stick stick,
      Gamepad::StickDirection direction) const noexcept = 0;

    /**
     * @brief Set the rumble motors on/off.
              To turn off the rumble effect, set the values to 0 for both motors.
              The left-motor is the low-frequency / strong motor, and the right motor is the high-frequency / weak motor
              Input values to be between 0 - 1
     * @param leftMotor Gamepad Left Motor
     * @param rightMotor Gamepad Right Motor
     */
    virtual void SetRumble(float leftMotor, float rightMotor) noexcept = 0;

    /**
     * @brief Get rumble values on both motors
     * @param leftValue [output] left motor value
     * @param rightValue [output] right motor value
     */
    virtual void GetRumble(float& leftValue, float& rightValue) const noexcept = 0;

    /**
     * @brief Get Controller device number
     */
    virtual GamepadDevice GetDeviceNumber() const noexcept = 0;
  };

} } }