#pragma once

#include <engine/input/controller/controller_interface.h>

namespace Reimagine { namespace Engine { namespace Input { namespace Controller {

  class ControllerManagerInterface : ReCommon::Noncopyable
  {
  public:
    virtual ~ControllerManagerInterface() = default;

    /**
     * @brief Finds the first connected controller. If there's none, INVALID is returned
     */
    virtual GamepadDevice IsAnyConnected() const noexcept = 0;

    /**
     * @brief Gets the controller
     * @param deviceNumber Gamepad Device number
     */
    virtual ControllerInterface& GetController(GamepadDevice deviceNumber) noexcept = 0;

    /**
     * @brief Gets the controller
     * @param deviceNumber Gamepad Device number
     */
    virtual const ControllerInterface& GetController(GamepadDevice deviceNumber) const noexcept = 0;

    /**
     * @brief Update all connected controllers
     */
    virtual void Update() noexcept = 0;

    /**
     * @brief Reset all controller states
     */
    virtual void Reset() noexcept = 0;
  };

} } } }