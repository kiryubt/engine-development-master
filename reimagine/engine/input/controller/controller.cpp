#include <engine/input/controller/controller.h>

namespace Reimagine { namespace Engine { namespace Input { namespace Controller {

  namespace
  {
    static constexpr uint8_t FlagConnected = 1 << 0;
    static constexpr uint8_t FlagRumble = 1 << 1;
    inline constexpr uint32_t ButtonToFlag(uint8_t button) noexcept { return 1 << button; }
  }

  Controller::Controller(GamepadDevice deviceNumber)
    : DeviceNumber{deviceNumber}
  {
  }

  Controller::~Controller()
  {
    ResetState();
  }

  void Controller::ResetState() noexcept
  {
    for (auto& elem : State.Sticks)
      elem = GamepadAxis{};

    for (auto& elem : State.Triggers)
      elem = GamepadTriggerInfo{};

    SetRumble(0, 0);
    State.CurrentButtons = 0;
    State.LastButtons = 0;
    State.PluggedIn = false;
    State.Flags = 0;
    State.PreviousFlags = 0;
  }

  void Controller::Update() noexcept
  {
    State.LastButtons = State.CurrentButtons;
    UpdateDevice();
    if (State.Flags & FlagConnected)
    {
      UpdateStick(State.Sticks[Common::to_underlying_type(Gamepad::Stick::STICKLEFT)], GamepadDeadzoneLeftStickValue);
      UpdateStick(State.Sticks[Common::to_underlying_type(Gamepad::Stick::STICKRIGHT)], GamepadDeadzoneRightStickValue);

      UpdateTrigger(State.Triggers[Common::to_underlying_type(Gamepad::Trigger::TRIGGERLEFT)]);
      UpdateTrigger(State.Triggers[Common::to_underlying_type(Gamepad::Trigger::TRIGGERRIGHT)]);
    }
  }

  bool Controller::IsConnected() const noexcept
  {
    return State.Flags && FlagConnected;
  }

  bool Controller::PluggedIn() noexcept
  {
    if (State.PluggedIn)
    {
      // @TODO: If this function is used in every loop, consider removing this log
      LOG(WARN) << "Controller " << Common::to_underlying_type(DeviceNumber) << " is already connected.";
      return false;
    }

    if (!(!State.PreviousFlags && (State.Flags & FlagConnected)))
      return false;

    LOG(INFO) << "Controller " << Common::to_underlying_type(DeviceNumber) << " connected.";
    State.PluggedIn = true;
    return true;
  }

  bool Controller::PluggedOut() noexcept
  {
    if (!State.PluggedIn)
    {
      // @TODO: If this function is used in every loop, consider removing this log
      LOG(WARN) << "Controller " << Common::to_underlying_type(DeviceNumber) << " is not connected.";
      return false;
    }

    if (!((State.PreviousFlags & FlagConnected) && !State.Flags))
      return false;

    LOG(INFO) << "Controller " << Common::to_underlying_type(DeviceNumber) << " disconnected.";
    State.PluggedIn = false;
    return true;
  }

  bool Controller::ButtonPressed(Gamepad::Button button) const noexcept
  {
    return (State.CurrentButtons & ButtonToFlag(Common::to_underlying_type(button)));
  }

  bool Controller::ButtonReleased(Gamepad::Button button) const noexcept
  {
    return !(State.CurrentButtons & ButtonToFlag(Common::to_underlying_type(button)))
      && (State.LastButtons & ButtonToFlag(Common::to_underlying_type(button)));
  }

  bool Controller::ButtonOnHold(Gamepad::Button button) const noexcept
  {
    return !(State.LastButtons & ButtonToFlag(Common::to_underlying_type(button)))
      && (State.CurrentButtons & ButtonToFlag(Common::to_underlying_type(button)));
  }

  int Controller::TriggerValueRaw(Gamepad::Trigger trigger) const noexcept
  {
    return State.Triggers[Common::to_underlying_type(trigger)].Value;
  }

  float Controller::TriggerValueNormalized(Gamepad::Trigger trigger) const noexcept
  {
    return State.Triggers[Common::to_underlying_type(trigger)].NormalizedValue;
  }

  bool Controller::TriggerPressed(Gamepad::Trigger trigger) const noexcept
  {
    const auto& triggerInfo = State.Triggers[Common::to_underlying_type(trigger)];
    return triggerInfo.Ongoing;
  }

  bool Controller::TriggerReleased(Gamepad::Trigger trigger) const noexcept
  {
    const auto& triggerInfo = State.Triggers[Common::to_underlying_type(trigger)];
    return triggerInfo.CurrentPressed
      && !triggerInfo.Ongoing
      && triggerInfo.LastOngoing;
  }

  bool Controller::TriggerOnHold(Gamepad::Trigger trigger) const noexcept
  {
    const auto& triggerInfo = State.Triggers[Common::to_underlying_type(trigger)];
    return triggerInfo.CurrentPressed
      && !triggerInfo.LastOngoing
      && triggerInfo.Ongoing;
  }

  void Controller::GetStickXYCurrentValues(Gamepad::Stick stick, int& x, int& y) const noexcept
  {
    x = State.Sticks[Common::to_underlying_type(stick)].X;
    y = State.Sticks[Common::to_underlying_type(stick)].Y;
  }

  void Controller::GetStickNormalizedXY(Gamepad::Stick stick, float& x, float& y) const noexcept
  {
    x = State.Sticks[Common::to_underlying_type(stick)].NormalizedX;
    y = State.Sticks[Common::to_underlying_type(stick)].NormalizedY;
  }

  void Controller::GetStickXYPreviousValues(Gamepad::Stick stick, int& x, int& y) const noexcept
  {
    x = State.Sticks[Common::to_underlying_type(stick)].LastX;
    y = State.Sticks[Common::to_underlying_type(stick)].LastY;
  }

  float Controller::GetStickMagnitude(Gamepad::Stick stick) const noexcept
  {
    return State.Sticks[Common::to_underlying_type(stick)].Magnitude;
  }

  Gamepad::StickDirection Controller::GetStickDirection(Gamepad::Stick stick) const noexcept
  {
    return State.Sticks[Common::to_underlying_type(stick)].CurrentDirection;
  }

  float Controller::GetStickAngle(Gamepad::Stick stick) const noexcept
  {
    return State.Sticks[Common::to_underlying_type(stick)].Angle;
  }

  bool Controller::GetStickDirectionTriggered(
    Gamepad::Stick stick,
    Gamepad::StickDirection direction) const noexcept
  {
    return State.Sticks[Common::to_underlying_type(stick)].CurrentDirection == direction
      && State.Sticks[Common::to_underlying_type(stick)].CurrentDirection != State.Sticks[Common::to_underlying_type(stick)].LastDirection;
  }

  void Controller::SetRumble(float leftMotor, float rightMotor) noexcept
  {
    static constexpr uint32_t rumbleValue = 65535;

    if ((State.Flags & FlagRumble) != 0)
    {
      XINPUT_VIBRATION vibration{};
      vibration.wLeftMotorSpeed = static_cast<WORD>(leftMotor * rumbleValue);
      vibration.wRightMotorSpeed = static_cast<WORD>(rightMotor * rumbleValue);
      State.LeftMotorValue = vibration.wLeftMotorSpeed;
      State.RightMotorValue = vibration.wRightMotorSpeed;
      XInputSetState(Common::to_underlying_type(DeviceNumber), &vibration);
    }
  }

  void Controller::GetRumble(float& leftValue, float& rightValue) const noexcept
  {
    static constexpr float rumbleValue = 1.f / 65535;
    leftValue = static_cast<float>(State.LeftMotorValue) * rumbleValue,
    rightValue = static_cast<float>(State.RightMotorValue) * rumbleValue;
  }

  void Controller::UpdateDevice() noexcept
  {
    State.PreviousFlags = State.Flags;

    XINPUT_STATE state{};
    // Disconnected
    if (XInputGetState(Common::to_underlying_type(DeviceNumber), &state))
    {
      State.Flags = 0;
      return;
    }

    // Reset device if not connected
    if (!(State.Flags & FlagConnected))
      ResetState();

    State.Flags |= FlagConnected | FlagRumble;

    State.CurrentButtons = state.Gamepad.wButtons;
    State.Triggers[Common::to_underlying_type(Gamepad::Trigger::TRIGGERLEFT)].Value = state.Gamepad.bLeftTrigger;
    State.Triggers[Common::to_underlying_type(Gamepad::Trigger::TRIGGERLEFT)].Value = state.Gamepad.bRightTrigger;
    
    State.Sticks[Common::to_underlying_type(Gamepad::Stick::STICKLEFT)].LastX = State.Sticks[Common::to_underlying_type(Gamepad::Stick::STICKLEFT)].X;
    State.Sticks[Common::to_underlying_type(Gamepad::Stick::STICKLEFT)].X = state.Gamepad.sThumbLX;
    State.Sticks[Common::to_underlying_type(Gamepad::Stick::STICKLEFT)].LastY = State.Sticks[Common::to_underlying_type(Gamepad::Stick::STICKLEFT)].Y;
    State.Sticks[Common::to_underlying_type(Gamepad::Stick::STICKLEFT)].Y = state.Gamepad.sThumbLY;

    State.Sticks[Common::to_underlying_type(Gamepad::Stick::STICKRIGHT)].LastX = State.Sticks[Common::to_underlying_type(Gamepad::Stick::STICKRIGHT)].X;
    State.Sticks[Common::to_underlying_type(Gamepad::Stick::STICKRIGHT)].X = state.Gamepad.sThumbRX;
    State.Sticks[Common::to_underlying_type(Gamepad::Stick::STICKRIGHT)].LastY = State.Sticks[Common::to_underlying_type(Gamepad::Stick::STICKRIGHT)].Y;
    State.Sticks[Common::to_underlying_type(Gamepad::Stick::STICKRIGHT)].Y = state.Gamepad.sThumbRY;
  }

  void Controller::UpdateStick(GamepadAxis& axis, float deadzone) noexcept
  {
    // @TODO: Change to constexpr of Pi when math library is available
    const float Pi = std::atan(1.f) * 4.f;
    const float StickMaxValue = 32767.f;
    const float QuarterPi = Pi * 4 * 0.25f;
    const float ThreeQuarterPi = 3 * Pi * 0.25f;

    axis.Magnitude = sqrtf(static_cast<float>(axis.X * axis.X + axis.Y * axis.Y));
    if (axis.Magnitude > deadzone)
    {
      axis.Magnitude = axis.Magnitude > StickMaxValue ? StickMaxValue : axis.Magnitude;
      axis.NormalizedX = static_cast<float>(axis.X) / axis.Magnitude;
      axis.NormalizedY = static_cast<float>(axis.Y) / axis.Magnitude;

      // Adjust length for deadzone and find normalized magnitude
      axis.Magnitude -= deadzone;
      axis.Magnitude /= (StickMaxValue - deadzone);

      // Angle of the stick (in Radius)
      axis.Angle = std::atan2f(static_cast<float>(axis.Y), static_cast<float>(axis.X));
    }
    else
    {
      axis.X = axis.Y = 0;
      axis.NormalizedX = axis.NormalizedY = 0;
      axis.Magnitude = axis.Angle = 0;
    }

    axis.LastDirection = axis.CurrentDirection;
    axis.CurrentDirection = Gamepad::StickDirection::CENTER;

    // Check direction to determine if it's non-centered
    if (axis.Magnitude)
    {
      if (axis.Angle >= QuarterPi && axis.Angle < ThreeQuarterPi)
        axis.CurrentDirection = Gamepad::StickDirection::UP;
      else if (axis.Angle >= -ThreeQuarterPi && axis.Angle < -QuarterPi)
        axis.CurrentDirection = Gamepad::StickDirection::DOWN;
      else if (axis.Angle >= ThreeQuarterPi || axis.Angle < -ThreeQuarterPi)
        axis.CurrentDirection = Gamepad::StickDirection::LEFT;
      else // if (axis.Angle < QuarterPi && axis.Angle >= -QuarterPi)
        axis.CurrentDirection = Gamepad::StickDirection::RIGHT;
    }
  }

  void Controller::UpdateTrigger(GamepadTriggerInfo& triggerInfo) noexcept
  {
    triggerInfo.LastPressed = triggerInfo.CurrentPressed;
    triggerInfo.LastNormalizedValue = triggerInfo.NormalizedValue;
    triggerInfo.LastOngoing = triggerInfo.Ongoing;

    if (triggerInfo.Value > GamepadDeadzoneTriggerValue)
    {
      triggerInfo.NormalizedValue =
        static_cast<float>(triggerInfo.Value - GamepadDeadzoneTriggerValue)
        / (255.f - GamepadDeadzoneTriggerValue);
      triggerInfo.CurrentPressed = true;

      // Custom configuration for our trigger states
      if (!triggerInfo.Ongoing
        && triggerInfo.NormalizedValue > 0.92f
        && triggerInfo.LastNormalizedValue <= 0.92f)
        triggerInfo.Ongoing = true;

      else if (triggerInfo.Ongoing
        && triggerInfo.NormalizedValue < 0.45f
        && triggerInfo.LastNormalizedValue >= 0.45f)
        triggerInfo.Ongoing = false;
    }
    else
    {
      triggerInfo.Value = 0;
      triggerInfo.NormalizedValue = 0;
      triggerInfo.CurrentPressed = false;
      triggerInfo.Ongoing = false;
    }
  }

  GamepadDevice Controller::GetDeviceNumber() const noexcept
  {
    return DeviceNumber;
  }

} } } }