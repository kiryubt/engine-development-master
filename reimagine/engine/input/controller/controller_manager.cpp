#include <engine/common/dialogue/dialogue.h>
#include <engine/input/controller/controller.h>
#include <engine/input/controller/controller_manager.h>

namespace Reimagine { namespace Engine { namespace Input { namespace Controller {

  ControllerManager::ControllerManager()
  {
    auto size = Controllers.size();
    for (decltype(size) i = 0; i != size; ++i)
      Controllers[i] = std::make_unique<Controller>(static_cast<GamepadDevice>(i));
  }

  GamepadDevice ControllerManager::IsAnyConnected() const noexcept
  {
    for (const auto& controller : Controllers)
      if (controller && controller->IsConnected())
        return controller->GetDeviceNumber();

    return GamepadDevice::INVALID;
  }

  ControllerInterface& ControllerManager::GetController(GamepadDevice index) noexcept
  {
    ASSERT(index >= GamepadDevice::COUNT, "Index exceeded GamepadDevice::COUNT");
    ASSERT(Controllers[Common::to_underlying_type(index)].get(), "Controller is not initialized.");
    return *Controllers[Common::to_underlying_type(index)];
  }

  const ControllerInterface& ControllerManager::GetController(GamepadDevice index) const noexcept
  {
    ASSERT(index >= GamepadDevice::COUNT, "Index exceeded GamepadDevice::COUNT");
    ASSERT(Controllers[Common::to_underlying_type(index)].get(), "Controller is not initialized.");
    return *Controllers[Common::to_underlying_type(index)];
  }

  void ControllerManager::Update() noexcept
  {
    for (auto& controller : Controllers)
      if (controller)
        controller->Update();
  }

  void ControllerManager::Reset() noexcept
  {
    for (auto& controller : Controllers)
      if (controller)
        controller->ResetState();
  }

} } } }