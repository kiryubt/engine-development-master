/**
 * @file    input.cpp
 * @brief
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#include <engine/input/input_system.h>
#include <engine/window/glfw_window.h>

namespace Reimagine { namespace Engine { namespace Input {

  InputSystem::InputSystem(std::unique_ptr<Controller::ControllerManagerInterface> controllerManager)
    : KeysOnHold{false}
    , KeysPressed{false}
    , KeysReleased{false}
    , MouseStateTimer{0}
    , ControllerManager{std::move(controllerManager)}
  {
    ASSERT(ControllerManager != nullptr, "Invalid Controller Manager");
  }

  void InputSystem::Update()
  {
    UpdateKeyStates();

    ControllerManager->Update();
  }

  void InputSystem::ToggleMouseDisplay(bool value) const noexcept
  {
    // ShowCursor(bool) API is counter based function
    // Hence it is required to check for the resulting counter value
    auto result = ShowCursor(value);
    while (value ? (result < 0) : (result >= 0))
      ShowCursor(value);
  }

  bool InputSystem::IsMouseClicked(Mouse key) const noexcept
  {
    return KeysPressed[Common::to_underlying_type(key)];
  }

  bool InputSystem::IsMouseReleased(Mouse key) const noexcept
  {
    return KeysReleased[Common::to_underlying_type(key)];
  }

  bool InputSystem::IsMouseOnHold(Mouse key) const noexcept
  {
    return KeysOnHold[Common::to_underlying_type(key)];
  }

  bool InputSystem::IsKeyPressed(Keyboard key) const noexcept
  {
    return KeysPressed[Common::to_underlying_type(key)];
  }

  bool InputSystem::IsKeyReleased(Keyboard key) const noexcept
  {
    return KeysReleased[Common::to_underlying_type(key)];
  }

  bool InputSystem::IsKeyOnHold(Keyboard key) const noexcept
  {
    return KeysOnHold[Common::to_underlying_type(key)];
  }

  bool InputSystem::CheckCombination(Keyboard holdKey, Keyboard pressKey) const noexcept
  {
    return KeysOnHold[Common::to_underlying_type(holdKey)]
      && KeysPressed[Common::to_underlying_type(pressKey)];
  }

  void InputSystem::Reset() noexcept
  {
    ResetKeyStates();
    ControllerManager->Reset();
  }

  bool InputSystem::operator==(const InputSystem& other) const noexcept
  {
    return KeysOnHold == other.KeysOnHold
      && KeysPressed == other.KeysPressed
      && KeysReleased == other.KeysReleased
      && ControllerManager == other.ControllerManager;
  }

  void InputSystem::UpdateMouseInfo() noexcept
  {
    POINT point{};
    if (!GetCursorPos(&point))
      return;

    const auto windowSystem = Ipc::GetWindow();
    const auto& windowInfo = windowSystem->GetRawWindowInformation();
    const auto& windowWidth = windowInfo.GetWidth();
    const auto& windowHeight = windowInfo.GetHeight();

    TheMouseInfo.NormalizedPosition = glm::vec2{
      point.x / (windowWidth * 0.5f) - 1,
      -point.y / (windowHeight * 0.5f) + 1};
    TheMouseInfo.RawPosition = glm::vec2{point.x, point.y};

    // "Finite State Machine" to determine if it was clicked
    // Referenced from https://stackoverflow.com/questions/15226791/distinguish-single-click-from-double-click-c
    switch(TheMouseInfo.States)
    {
    case MouseClickStates::NONE:
      if (IsMouseClicked(Mouse::LEFTCLICK))
        TheMouseInfo.States = MouseClickStates::SINGLE;
      break;

    case MouseClickStates::SINGLE:
      if (MouseStateTimer < MaxDoubleClickInterval)
      {
        MouseStateTimer += Ipc::GetDeltaTime();
        if (IsMouseClicked(Mouse::LEFTCLICK))
          TheMouseInfo.States = MouseClickStates::DOUBLE;
      }
      else
      {
        TheMouseInfo.States = MouseClickStates::NONE;
        MouseStateTimer = 0;
      }
      break;

    case MouseClickStates::DOUBLE:
      TheMouseInfo.States = MouseClickStates::NONE;
      MouseStateTimer = 0;
      break;
    }
  }

  void InputSystem::UpdateKeyStates() noexcept
  {
    auto keyPressed = false;
    for (uint8_t i = 0; i != MaxKeys; ++i)
    {
      keyPressed = KeysOnHold[i];
      auto keyState = GetAsyncKeyState(i);
      // low-order bit == 1 (key pressed)
      KeysPressed[i] = !keyPressed && keyState;
      // high-order bit == 1 (key is on hold)
      KeysOnHold[i] = keyState;
      KeysReleased[i] = keyPressed && !KeysOnHold[i];
    }
  }

  void InputSystem::ResetKeyStates() noexcept
  {
    for (uint8_t i = 0; i != MaxKeys; ++i)
    {
      KeysOnHold[i] = false;
      KeysPressed[i] = false;
      KeysReleased[i] = false;
    }

    ControllerManager->Reset();
  }

} } }