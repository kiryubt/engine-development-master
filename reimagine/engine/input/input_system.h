/**
 * @file    input.h
 * @brief   input system
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#pragma once

#include <engine/input/controller/controller_manager_interface.h>
#include <engine/input/input_system_interface.h>
#include <engine/system/core.h>
#include <atomic>

namespace Reimagine { namespace Engine { namespace Input {

  class InputSystem final : public InputSystemInterface
  {
  public:
    InputSystem(std::unique_ptr<Controller::ControllerManagerInterface> controllerManager);

    /**
     * @brief Calls for update on every frame
     */
    void Update() override;

    /**
     * @brief Toggle the mouse display
     * @param value to show or not show the mouse 
     */
    void ToggleMouseDisplay(bool value) const noexcept override;

    /**
     * @brief Determine if a mouse key is pressed
     * @param key Mouse key
     */
    bool IsMouseClicked(Mouse key) const noexcept override;

    /**
     * @brief Determine if a mouse key is released
     * @param key Mouse key
     */
    bool IsMouseReleased(Mouse key) const noexcept override;

    /**
     * @brief Determine if a mouse key is on hold
     * @param key Mouse key
     */
    bool IsMouseOnHold(Mouse key) const noexcept override;

    /**
     * @brief Determine if a key is pressed
     * @param key keyboard key
     */
    bool IsKeyPressed(Keyboard key) const noexcept override;

    /**
     * @brief Determine if a key is released
     * @param key keyboard key
     */
    bool IsKeyReleased(Keyboard key) const noexcept override;

    /**
     * @brief Determine if a key is on hold (after key pressed)
     * @param key keyboard key
     */
    bool IsKeyOnHold(Keyboard key) const noexcept override;

    /**
     * @brief Determine if a key combination (2 keys) are triggered
     * @param holdKey first key that is currently holding
     * @param pressKey second key that is pressed
     */
    bool CheckCombination(Keyboard holdKey, Keyboard pressKey) const noexcept override;

    /**
     * @brief Reset all key, mouse and controller states
     */
    void Reset() noexcept override;

    bool operator==(const InputSystem& other) const noexcept;

  private:
    using KeysContainer = std::array<bool, MaxKeys>;

    struct MouseInfo
    {
      MouseClickStates States = MouseClickStates::NONE;
      glm::vec2 NormalizedPosition;
      glm::vec2 RawPosition;
    };

    /**
     * @brief Update mouse info
     */
    void UpdateMouseInfo() noexcept;

    /**
     * @brief Update all key states
     */
    void UpdateKeyStates() noexcept;
    
    /**
     * @brief Reset all key states
     */
    void ResetKeyStates() noexcept;

    KeysContainer KeysOnHold;
    KeysContainer KeysPressed;
    KeysContainer KeysReleased;
    MouseInfo TheMouseInfo;
    std::unique_ptr<Controller::ControllerManagerInterface> ControllerManager;
    float MouseStateTimer;
  };

} } }