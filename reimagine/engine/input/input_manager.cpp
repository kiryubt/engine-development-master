#include <engine/input/input_manager.h>
#include <engine/input/controller/controller_manager.h>
#include <engine/input/input_system.h>

namespace Reimagine { namespace Engine { namespace Input {

  InputManager::InputManager(System::Core* engine)
    : SystemBaseT{System::SystemTypes::InputSystem, engine}
  {
  }

  void InputManager::Initialise()
  {
    // nothing to do
  }

  void InputManager::Update()
  {
    for (auto& [key, input] : InputSystems)
      input->Update();
  }

  void InputManager::PostUpdate()
  {
    // nothing to do
  }

  void InputManager::Destroy()
  {
    for (auto& [key, input] : InputSystems)
      input->Reset();
    InputSystems.clear();
  }

  void InputManager::RegisterInputSystem(const std::string& name, bool setActive)
  {
    if (name.empty())
    {
      LOG(ERR, inputManager) << "Unable to register input system. Invalid input name";
      return;
    }

    std::lock_guard<std::mutex> lockGuard{Mutex};
    auto controllerManager = std::make_unique<Controller::ControllerManager>();
    InputSystems[name] = std::make_shared<InputSystem>(std::move(controllerManager));
    if (setActive)
    {
      ActiveInputSystem = InputSystems[name];
      ActiveInputSystem->Reset();
    }
  }

  void InputManager::UnregisterInputSystem(const std::string& name)
  {
    if (name.empty())
    {
      LOG(ERR, inputManager) << "Unable to unregister input system. Invalid input name";
      return;
    }

    auto iter = InputSystems.find(name);
    if (iter == InputSystems.end())
    {
      LOG(ERR, inputManager)
        << "Unable to unregister input system. "
        << name
        << " is not found";
      return;
    }

    std::lock_guard<std::mutex> lockGuard{Mutex};
    auto& [key, input] = *iter;
    input->Reset();
    InputSystems.erase(iter);
  }

  void InputManager::SetActiveInputSystem(const std::optional<std::string>& name)
  {
    std::lock_guard<std::mutex> lockGuard{Mutex};
    if (!name)
    {
      ActiveInputSystem->Reset();
      ActiveInputSystem = nullptr;
      return;
    }

    auto iter = InputSystems.find(name.value());
    if (iter == InputSystems.end())
    {
      LOG(ERR, inputManager)
        << "Unable to set active input system. "
        << name.value()
        << " is not found";
      return;
    }

    ActiveInputSystem->Reset();
    auto& [key, input] = *iter;
    ActiveInputSystem = input;
  }

  std::shared_ptr<InputSystemInterface> InputManager::GetActiveInputSystem() noexcept
  {
    std::lock_guard<std::mutex> lockGuard{Mutex};
    return ActiveInputSystem;
  }

} } }