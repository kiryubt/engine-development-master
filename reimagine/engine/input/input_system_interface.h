#pragma once

#include <engine/input/input_common.h>
#include <re_common/noncopyable.h>
#include <cstdint>

namespace Reimagine { namespace Engine { namespace Input {

  static constexpr float MaxDoubleClickInterval = 0.5f;
  static constexpr uint8_t MaxKeys = 255;

  enum class MouseClickStates
  {
    NONE,
    SINGLE,
    DOUBLE,
  };

  class InputSystemInterface : ReCommon::Noncopyable
  {
  public:
    /**
     * @brief Calls for update on every frame
     */
    virtual void Update() = 0;

    /**
     * @brief Toggle the mouse display
     * @param value to show or not show the mouse 
     */
    virtual void ToggleMouseDisplay(bool value) const noexcept = 0;

    /**
     * @brief Determine if a mouse key is pressed
     * @param key Mouse key
     */
    virtual bool IsMouseClicked(Mouse key) const noexcept = 0;

    /**
     * @brief Determine if a mouse key is released
     * @param key Mouse key
     */
    virtual bool IsMouseReleased(Mouse key) const noexcept = 0;

    /**
     * @brief Determine if a mouse key is on hold
     * @param key Mouse key
     */
    virtual bool IsMouseOnHold(Mouse key) const noexcept = 0;

    /**
     * @brief Determine if a key is pressed
     * @param key keyboard key
     */
    virtual bool IsKeyPressed(Keyboard key) const noexcept = 0;

    /**
     * @brief Determine if a key is released
     * @param key keyboard key
     */
    virtual bool IsKeyReleased(Keyboard key) const noexcept = 0;

    /**
     * @brief Determine if a key is on hold (after key pressed)
     * @param key keyboard key
     */
    virtual bool IsKeyOnHold(Keyboard key) const noexcept = 0;

    /**
     * @brief Determine if a key combination (2 keys) are triggered
     * @param holdKey first key that is currently holding
     * @param pressKey second key that is pressed
     */
    virtual bool CheckCombination(Keyboard holdKey, Keyboard pressKey) const noexcept = 0;

    /**
     * @brief Reset all key, mouse and controller states
     */
    virtual void Reset() noexcept = 0;
  };

} } }