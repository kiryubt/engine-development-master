/**
 * @file    input_common.h
 * @brief   global enums, values
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#pragma once

#include <cstdint>
#include <windows.h>

namespace Reimagine { namespace Engine { namespace Input {

  enum class Mouse : uint8_t
  {
    LEFTCLICK = VK_LBUTTON,
    RIGHTCLICK = VK_RBUTTON,
    MIDDLECLICK = VK_MBUTTON,
  };

  enum class Keyboard : uint8_t
  {
    BACK = VK_BACK,
    BACKSLASH = VK_OEM_2,
    CAPSLOCK = VK_CAPITAL,
    COMMA = VK_OEM_COMMA,
    DEL = VK_DELETE,
    DOWN = VK_DOWN,
    END = VK_END,
    ESCAPE = VK_ESCAPE,
    FRONTSLASH = VK_OEM_5,
    HOME = VK_HOME,
    INSERT = VK_INSERT,
    LALT = VK_LMENU,
    LBRACKET = VK_OEM_4,
    LCTRL = VK_LCONTROL,
    LEFT = VK_LEFT,
    LSHIFT = VK_LSHIFT,
    MINUS = VK_OEM_MINUS,
    PAGEDOWN = VK_NEXT,
    PAGEUP = VK_PRIOR,
    PAUSEBREAK = VK_PAUSE,
    PERIOD = VK_OEM_PERIOD,
    PLUS = VK_OEM_PLUS,
    PRINTSCREEN = VK_SNAPSHOT,
    QUOTE = VK_OEM_7,
    RALT = VK_RMENU,
    RBRACKET = VK_OEM_6,
    RCTRL = VK_RCONTROL,
    RETURN = VK_RETURN,
    RIGHT = VK_RIGHT,
    RSHIFT = VK_RSHIFT,
    SCROLLLOCK = VK_SCROLL,
    SEMICOLON = VK_OEM_1,
    SPACE = VK_SPACE,
    TAB = VK_TAB,
    TILDE = VK_OEM_3,
    UP = VK_UP,

    NUM0 = 0x30,
    NUM1 = 0x31,
    NUM2 = 0x32,
    NUM3 = 0x33,
    NUM4 = 0x34,
    NUM5 = 0x35,
    NUM6 = 0x36,
    NUM7 = 0x37,
    NUM8 = 0x38,
    NUM9 = 0x39,

    A = 0x41,
    B = 0x42,
    C = 0x43,
    D = 0x44,
    E = 0x45,
    F = 0x46,
    G = 0x47,
    H = 0x48,
    I = 0x49,
    J = 0x4A,
    K = 0x4B,
    L = 0x4C,
    M = 0x4D,
    N = 0x4E,
    O = 0x4F,
    P = 0x50,
    Q = 0x51,
    R = 0x52,
    S = 0x53,
    T = 0x54,
    U = 0x55,
    V = 0x56,
    W = 0x57,
    X = 0x58,
    Y = 0x59,
    Z = 0x5A,

    F1 = VK_F1,
    F2 = VK_F2,
    F3 = VK_F3,
    F4 = VK_F4,
    F5 = VK_F5,
    F6 = VK_F6,
    F7 = VK_F7,
    F8 = VK_F8,
    F9 = VK_F9,
    F10 = VK_F10,
    F11 = VK_F11,
    F12 = VK_F12,

    NUMPAD0 = VK_NUMPAD0,
    NUMPAD1 = VK_NUMPAD1,
    NUMPAD2 = VK_NUMPAD2,
    NUMPAD3 = VK_NUMPAD3,
    NUMPAD4 = VK_NUMPAD4,
    NUMPAD5 = VK_NUMPAD5,
    NUMPAD6 = VK_NUMPAD6,
    NUMPAD7 = VK_NUMPAD7,
    NUMPAD8 = VK_NUMPAD8,
    NUMPAD9 = VK_NUMPAD9,
    NUMMULTIPLY = VK_MULTIPLY,
    NUMPLUS = VK_ADD,
    NUMMINUS = VK_SUBTRACT,
    NUMPERIOD = VK_DECIMAL,
    NUMDIVIDE = VK_DIVIDE,
    NUMLOCK = VK_NUMLOCK,
  };

  struct Gamepad
  {
    enum class Button : uint8_t
    {
      DPADUP = 0,
      DPADDOWN = 1,
      DPADLEFT = 2,
      DPADRIGHT = 3,

      START = 4,
      BACK = 5,

      LEFTTHUMB = 6,
      RIGHTTHUMB = 7,

      LEFTSHOULDER = 8,
      RIGHTSHOULDER = 9,

      A = 12,
      B = 13,
      X = 14,
      Y = 15,
    };

    enum class Trigger : uint8_t
    {
      TRIGGERLEFT = 0,
      TRIGGERRIGHT = 1,

      TRIGGERCOUNT = 2
    };

    enum class Stick
    {
      STICKLEFT = 0,
      STICKRIGHT = 1,

      STICKCOUNT = 2
    };

    enum class StickDirection : uint8_t
    {
      CENTER = 0,
      UP = 1,
      DOWN = 2,
      LEFT = 3,
      RIGHT = 4,
    };
  };

} } }