#pragma once

#include <engine/input/input_system_interface.h>
#include <engine/input/controller/controller_manager_interface.h>
#include <engine/system/core.h>
#include <unordered_map>
#include <mutex>

namespace Reimagine { namespace Engine { namespace Input {

  class InputManager final : public System::SystemBase<InputManager>
  {
  public:
    InputManager(System::Core* engine);

    /**
     * @brief Initialise input manager
     */
    void Initialise();

    /**
     * @brief Updates input manager
     */
    void Update();

    /**
     * @brief Post logic update from Update()
     */
    void PostUpdate();

    /**
     * @brief Destructs input manager
     */
    void Destroy();

    /**
     * @brief Register Input System to Manager
     * @param name Name of the Input System
     * @param setActive Sets registered Input System as active input
     */
    void RegisterInputSystem(const std::string& name, bool setActive = true);

    /**
     * @brief Unregister Input System from the Manager
     * @param name name of the Input System
     */
    void UnregisterInputSystem(const std::string& name);

    /**
     * @brief Set the current active Input System
     *        If no name is passed in, removes the active Input System
     * @param name name of the Input System
     */
    void SetActiveInputSystem(const std::optional<std::string>& name);

    /**
     * @brief Returns the active input system, nullptr if none is active
     */
    std::shared_ptr<InputSystemInterface> GetActiveInputSystem() noexcept;

  private:
    using InputSystemCollection = std::unordered_map<std::string, std::shared_ptr<InputSystemInterface>>;

    InputSystemCollection InputSystems;
    std::shared_ptr<InputSystemInterface> ActiveInputSystem;
    std::mutex Mutex;
  };

} } }