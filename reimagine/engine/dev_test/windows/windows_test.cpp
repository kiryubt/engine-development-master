#include <engine/dev_test/windows/windows_test.h>

namespace Reimagine { namespace Engine { namespace DevTest { namespace Windows {

  void WindowsTest::TestInitialise()
  {
    WindowHandle = Ipc::GetWindow();
    auto fpsFunction =
      [this] () -> void
      {
        auto fps = Ipc::GetFPS();
        LOG(INFO, devTest) << "[windows test] FPS: " << fps;
      };
    GetFPSFunction = MakeTriggerNTimesFunction(fpsFunction, 10);
  }

  void WindowsTest::TestUpdate()
  {
    GetFPSFunction();
  }

  void WindowsTest::TestPostUpdate()
  {

  }

  void WindowsTest::TestDestroy()
  {

  }

} } } }