#pragma once

#include <engine/dev_test/dev_test_helpers.h>
#include <engine/window/glfw_window.h>

namespace Reimagine { namespace Engine { namespace DevTest { namespace Windows {

  struct WindowsTest final : TestInterface
  {
    void TestInitialise() override;
    void TestUpdate() override;
    void TestPostUpdate() override;
    void TestDestroy() override;

  private:
    Window::WindowSystem* WindowHandle;
    std::function<void()> GetFPSFunction;
  };

} } } }