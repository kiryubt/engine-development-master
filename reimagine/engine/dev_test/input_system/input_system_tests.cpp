#include <engine/dev_test/input_system/input_system_tests.h>
#include <engine/input/input_manager.h>
#include <engine/input/input_system.h>

namespace Reimagine { namespace Engine { namespace DevTest { namespace InputSystem {

  void InputSystemTest::TestInitialise()
  {
    InputManagerHandler = Ipc::GetInputManager();
    assert(InputManagerHandler);
    InputManagerHandler->RegisterInputSystem(inputName);
  }

  void InputSystemTest::TestUpdate()
  {
    auto testInput =
      [this] ()
      {
        auto& inputSystem = InputManagerHandler->GetActiveInputSystem();
        if (inputSystem->IsKeyPressed(Input::Keyboard::M))
          LOG(INFO, devTest) << "[input system tests] Keyboard M is pressed.";

        if (inputSystem->IsMouseClicked(Input::Mouse::LEFTCLICK))
          LOG(INFO, devTest) << "[input system tests] Left Click is pressed.";

        if (inputSystem->IsMouseOnHold(Input::Mouse::LEFTCLICK))
          LOG(INFO, devTest) << "[input system tests] Left Click is on hold.";

        if (inputSystem->IsMouseReleased(Input::Mouse::LEFTCLICK))
          LOG(INFO, devTest) << "[input system tests] Left Click is released.";
      };

    testInput();
  }

  void InputSystemTest::TestPostUpdate()
  {
  }

  void InputSystemTest::TestDestroy()
  {
    // Should produce error in log since Dev Test "system" logic is handled at very end
    InputManagerHandler->UnregisterInputSystem(inputName);
  }

} } } }