#pragma once

#include <engine/dev_test/dev_test_helpers.h>
#include <engine/input/input_manager.h>

namespace Reimagine { namespace Engine { namespace DevTest { namespace InputSystem {

  struct InputSystemTest final : TestInterface
  {
    void TestInitialise() override;
    void TestUpdate() override;
    void TestPostUpdate() override;
    void TestDestroy() override;

  private:
    Input::InputManager* InputManagerHandler;
    std::string inputName{"DevTest"};
  };

} } } }