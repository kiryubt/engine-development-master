#pragma once

#include <engine/dev_test/dev_test_helpers.h>
#include <engine/dev_test/input_system/input_system_tests.h>
#include <engine/dev_test/windows/windows_test.h>
#include <engine/system/system_interface.h>

namespace Reimagine { namespace Engine { namespace DevTest {

  class DevelopmentTesting final : public System::SystemBase<DevelopmentTesting>
  {
  public:
    DevelopmentTesting(Engine::System::Core* engine);

    /**
     * @brief Test for other systems that should be initialised
     */
    void Initialise();

    /**
     * @brief Test for other systems that should be updated
     */
    void Update();

    /**
     * @brief Test for other systems that should be post updated
     */
    void PostUpdate();

    /**
     * @brief Test for other systems that should be destroyed
     */
    void Destroy();

  private:
    using TestCollection = std::vector<std::unique_ptr<TestInterface>>;
    TestCollection Tests;

    // @TODO: To add your own testing "component" with the following conditions:
    // 1) Create "component" as std::unique_ptr<TESTCOMPONENT>
    // 2) TESTCOMPONENT should inherit from TestInterface
    // 3) Initialise TESTCOMPONENT in DevelopmentTesting constructor (i.e. std::make_unique<TESTCOMPONENT>(args...))
    // 4) Add to Tests (i.e. Tests.emplace_back(std::move(TESTCOMPONENTVARIABLE)))
    std::unique_ptr<Windows::WindowsTest> WindowTest;
    std::unique_ptr<InputSystem::InputSystemTest> InputTest;
  };

} } }