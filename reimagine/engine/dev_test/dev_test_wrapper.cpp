#include <engine/dev_test/dev_test_wrapper.h>

namespace Reimagine { namespace Engine { namespace DevTest {

  DevelopmentTesting::DevelopmentTesting(Engine::System::Core* engine)
    : SystemBaseT{System::SystemTypes::DevelopmentTestingSystem, engine}
    , Tests{}
    , WindowTest{std::make_unique<Windows::WindowsTest>()}
    , InputTest{std::make_unique<InputSystem::InputSystemTest>()}
  {
    Tests.emplace_back(std::move(WindowTest));
    Tests.emplace_back(std::move(InputTest));
  }

  void DevelopmentTesting::Initialise()
  {
    for (auto& test : Tests)
      test->TestInitialise();
  }

  void DevelopmentTesting::Update()
  {
    for (auto& test : Tests)
      test->TestUpdate();
  }
  
  void DevelopmentTesting::PostUpdate()
  {
    for (auto& test : Tests)
      test->TestPostUpdate();
  }

  void DevelopmentTesting::Destroy()
  {
    for (auto& test : Tests)
      test->TestDestroy();
  }

} } }