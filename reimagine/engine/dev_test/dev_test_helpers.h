#pragma once

namespace Reimagine { namespace Engine { namespace DevTest {

  namespace
  {
    template <typename FunctorT, size_t ... IndicesI>
    auto MakeTriggerHelper(FunctorT functor, uint16_t count, std::index_sequence<IndicesI...> /*sequence*/)
    {
      using argsType = typename Common::signature_trait<FunctorT>::args_type;
      using returnType = typename Common::signature_trait<FunctorT>::return_type;
      auto wrapperFunction =
        [functor, count, triggerCount = 0] (std::tuple_element_t<IndicesI, argsType>&& ... arguments) mutable -> returnType
        {
          if (triggerCount >= count)
          {
            if constexpr (std::is_void_v<returnType>)
              return;
            else
              return returnType{};
          }
          ++triggerCount;
          return functor(std::forward<decltype(arguments)>(arguments)...);
        };
      return wrapperFunction;
    }
  }

  struct TestInterface : ReCommon::Noncopyable
  {
    virtual void TestInitialise() = 0;
    virtual void TestUpdate() = 0;
    virtual void TestPostUpdate() = 0;
    virtual void TestDestroy() = 0;
  };

  template <typename FunctorT>
  auto MakeTriggerOnceFunction(FunctorT functor)
  {
    using argsIndexSequenceType = typename Common::signature_trait<FunctorT>::args_index_type;
    return MakeTriggerHelper(functor, 1, argsIndexSequenceType{});
  }

  template <typename FunctorT>
  auto MakeTriggerNTimesFunction(FunctorT functor, uint16_t count)
  {
    using argsIndexSequenceType = typename Common::signature_trait<FunctorT>::args_index_type;
    return MakeTriggerHelper(functor, count, argsIndexSequenceType{});
  }

} } }