
#include <engine/window/glfw_window.h>

namespace Reimagine { namespace Engine { namespace Window {

  namespace
  {
    void GlfwErrorCallback(int error, const char* description)
    {
      LOG(ERR, window) << "GLFW encountered an error with error code: " << error << ", message: " << description;
    }

    void GlfwMonitorCallback(GLFWmonitor* monitor, int event)
    {
      auto windowSystem = Ipc::GetWindow();
      windowSystem->HandleMonitorCallback(monitor, event);
    }

    void GlfwDragDropCallback(GLFWwindow* window, int count, const char** paths)
    {
      auto windowSystem = Ipc::GetWindow();
      windowSystem->HandleDragDropCallback(window, count, paths);
    }
  }

  GLFWwindow* WindowInformation::GetHandle() const noexcept
  {
    return WindowHandler.Handle;
  }

  const std::string& WindowInformation::GetTitle() const noexcept
  {
    return WindowHandler.Title;
  }

  uint32_t WindowInformation::GetWidth() const noexcept
  {
    return WindowHandler.Width;
  }

  uint32_t WindowInformation::GetHeight() const noexcept
  {
    return WindowHandler.Height;
  }

  glm::ivec2 WindowInformation::GetScreenMousePosition() const noexcept
  {
    double xPos;
    double yPos;
    glfwGetCursorPos(WindowHandler.Handle, &xPos, &yPos);
    return {static_cast<int32_t>(xPos), static_cast<int32_t>(yPos)};
  }

  glm::ivec2 WindowInformation::GetWindowPosition() const noexcept
  {
    int xPos;
    int yPos;
    glfwGetWindowPos(WindowHandler.Handle, &xPos, &yPos);
    return {xPos, yPos};
  }

  void WindowSystem::Initialise()
  {
    glfwSetErrorCallback(GlfwErrorCallback);

    int major;
    int minor;
    int revision;
    glfwGetVersion(&major, &minor, &revision);
    std::stringstream glfwVersion;
    glfwVersion << major << "." << minor << "." << revision;
    LOG(DEBUG, window) << "GLFW Version: v" << glfwVersion.str();
    if (!glfwInit())
    {
      LOG(ERR, window) << "GLFW Initialisation failed.";
      std::exit(EXIT_FAILURE);
    }

    int32_t count = 0;
    auto monitors = glfwGetMonitors(&count);
    if (!count)
    {
      LOG(ERR, window) << "No monitors are connected.";
      std::exit(EXIT_FAILURE);
    }

    TheMonitors.resize(count);
    std::uninitialized_default_construct(std::begin(TheMonitors), std::end(TheMonitors));
    for (int32_t index = 0; index != count; ++index)
      TheMonitors[index] = monitors[index];

    auto width = Width;
    auto height = Height;
    GLFWmonitor* monitor = nullptr;
    if (Flags & WindowFlags::FULLSCREEN)
    {
      LOG(DEBUG, window) << "Initialising with full screen mode";
      monitor = TheMonitors[0];
    }
    else if (Flags & WindowFlags::BORDERLESS)
    {
      LOG(DEBUG, window) << "Initialising with borderless full screen mode";
      auto videoMode = glfwGetVideoMode(TheMonitors[0]);
      glfwWindowHint(GLFW_RED_BITS, videoMode->redBits);
      glfwWindowHint(GLFW_GREEN_BITS, videoMode->greenBits);
      glfwWindowHint(GLFW_BLUE_BITS, videoMode->blueBits);
      glfwWindowHint(GLFW_REFRESH_RATE, videoMode->refreshRate);
      width = videoMode->width;
      height = videoMode->height;
      monitor = TheMonitors[0];
    }

    Handle = glfwCreateWindow(width, height, Title.c_str(), monitor, nullptr);
    if (!Handle)
    {
      LOG(ERR, window) << "Unable to create GLFW window.";
      glfwTerminate();
      std::exit(EXIT_FAILURE);
    }
    glfwSetWindowAspectRatio(Handle, width, height);

    if (Flags & WindowFlags::WINDOWED)
    {
      LOG(DEBUG, window) << "Initialising with windowed mode";
      auto videoMode = glfwGetVideoMode(TheMonitors[0]);
      const auto centerXPos = (videoMode->width >> 1) - (width >> 1);
      const auto centerYPos = (videoMode->height >> 1) - (height >> 1);
      glfwSetWindowPos(Handle, centerXPos, centerYPos);
    }
    glfwSetMonitorCallback(GlfwMonitorCallback);
    glfwSetDropCallback(Handle, GlfwDragDropCallback);
    glfwMakeContextCurrent(Handle);

    LOG(INFO, window) << "GLFW window created. Window System initialised.";
  }

  void WindowSystem::Update()
  {
    PollEvents();
  }

  void WindowSystem::PostUpdate()
  {
    if (glfwWindowShouldClose(Handle))
      Engine->Shutdown();
  }

  void WindowSystem::Destroy()
  {
    glfwTerminate();
    Handle = nullptr;
    TheMonitors.clear();
    LOG(INFO, window) << "GFLW window terminated. Window System destroyed.";
  }

  void WindowSystem::PollEvents()
  {
    // @TODO: Zhixian, remove graphical coes here once you have your
    // graphical system ready
    glClear(GL_COLOR_BUFFER_BIT);
    glfwSwapBuffers(Handle);

    glfwPollEvents();
  }

  const WindowInformation& WindowSystem::GetRawWindowInformation() const noexcept
  {
    return RawInfo;
  }

  WindowInformation& WindowSystem::GetRawWindowInformation() noexcept
  {
    return RawInfo;
  }

  void WindowSystem::HandleMonitorCallback(GLFWmonitor* monitor, int event)
  {
    if (event == GLFW_CONNECTED)
    {
      auto iter = std::find_if(
        std::begin(TheMonitors),
        std::end(TheMonitors),
        [] (const GLFWmonitor* theMonitor)
        {
          return !theMonitor;
        });
      if (iter == std::end(TheMonitors))
        TheMonitors.emplace_back(monitor);
      else
        (*iter) = monitor;
    }
    else if (event == GLFW_DISCONNECTED)
    {
      const std::string& monitorName = glfwGetMonitorName(monitor);
      auto iter = std::find_if(
        std::begin(TheMonitors),
        std::end(TheMonitors),
        [&monitorName] (GLFWmonitor* theMonitor)
        {
          return std::string{glfwGetMonitorName(theMonitor)} == monitorName;
        });

      if (iter != std::end(TheMonitors))
        (*iter) = nullptr;
    }
  }

  void WindowSystem::HandleDragDropCallback(GLFWwindow* window, int count, const char** paths)
  {
    for (int i = 0; i != count; ++i)
    {
      // @TODO: To handle paths that dropped into the window
      //        Most likely this should be captured by the Editor
      //        Handle this once the Editor class is available
      LOG(INFO, window) << "Path: " << paths[i];
    }
  }

} } }

Reimagine::Engine::Window::WindowFlags operator|(const Reimagine::Engine::Window::WindowFlags& lhs, const Reimagine::Engine::Window::WindowFlags& rhs)
{
  return static_cast<Reimagine::Engine::Window::WindowFlags>(
    Reimagine::Engine::Common::to_underlying_type(lhs) | Reimagine::Engine::Common::to_underlying_type(rhs));
}

bool operator&(const Reimagine::Engine::Window::WindowFlags& lhs, const Reimagine::Engine::Window::WindowFlags& rhs)
{
  return Reimagine::Engine::Common::to_underlying_type(lhs) & Reimagine::Engine::Common::to_underlying_type(rhs);
}