#pragma once

#include <engine/system/core.h>
#include <GLFW/glfw3.h>
#include <glm/vec2.hpp>

namespace Reimagine { namespace Engine { namespace Window {

  enum class WindowFlags
  {
    NONE = 0,
    // Resolution
    FULLSCREEN = 1 << 0,
    BORDERLESS = 1 << 1,
    WINDOWED = 1 << 2,
    // Render
    OPENGL = 1 << 3,
    VULKAN = 1 << 4
  };

  class WindowSystem;

  class WindowInformation final
  {
  public:

    WindowInformation(WindowSystem& windowHandler)
      : WindowHandler{windowHandler}
    {
    }

    /**
     * @brief Retrieves the window handle
     */
    GLFWwindow* GetHandle() const noexcept;

    /**
     * @brief Retrieves the title of the window
     */
    const std::string& GetTitle() const noexcept;

    /**
     * @brief Retrieves the width of the window
     */
    uint32_t GetWidth() const noexcept;

    /**
     * @brief Retrieves the height of the window
     */
    uint32_t GetHeight() const noexcept;

    /**
     * @brief Retrieves the mouse position from the main window
     *        Measured in screen coordinates but relative to top-left corner of the main window content area
     *        Does not include the menu bar
     */
    glm::ivec2 GetScreenMousePosition() const noexcept;

    /**
     * @brief Retrieves the main window position
     *        Measured in screen coordinates
     *        Does not include the menu bar
     */
    glm::ivec2 GetWindowPosition() const noexcept;

  private:
    WindowSystem& WindowHandler;
  };

  class WindowSystem final : public System::SystemBase<WindowSystem>
  {
  public:
    template <typename TitleT>
    WindowSystem(
      System::Core* engine,
      TitleT&& title,
      int32_t width,
      int32_t height,
      WindowFlags flags)
    : SystemBaseT{System::SystemTypes::WindowSystem, engine}
    , RawInfo{*this}
    , Width{width}
    , Height{height}
    , Title{title}
    , Flags{flags}
  {
  }

    /**
     * @brief Initialise the SDL Window and start creating
     */
    void Initialise();

    /**
     * @brief Updates the SDL Window, mostly capturing for window input
     */
    void Update();

    /**
     * @brief Post logic update from Update()
     */
    void PostUpdate();

    /**
     * @brief Destructs the SDL Window
     */
    void Destroy();

    /**
     * @brief Poll Events from windows
     */
    void PollEvents();

    /**
     * @brief Get Windows Raw Information
     */
    const WindowInformation& GetRawWindowInformation() const noexcept;

    /**
     * @brief Get Windows Raw Information
     */
    WindowInformation& GetRawWindowInformation() noexcept;

    /**
     * @brief Handles GLFW monitor callback function
     *        This is an internal function
     * @param monitor The monitor
     * @param event The event
     */
    void HandleMonitorCallback(GLFWmonitor* monitor, int event);

    /**
     * @brief Handles GLFW paths drop callback function
     *        This is an internal function
     * @param window The window
     * @param count The count
     * @param paths The paths
     */
    void HandleDragDropCallback(GLFWwindow* window, int count, const char** paths);

  private:
    using Monitors = std::vector<GLFWmonitor*>;

    Monitors TheMonitors;
    WindowInformation RawInfo;
    GLFWwindow* Handle;
    std::string Title;
    int32_t Width;
    int32_t Height;
    WindowFlags Flags;

    friend WindowInformation;
  };

} } }

Reimagine::Engine::Window::WindowFlags operator|(const Reimagine::Engine::Window::WindowFlags& lhs, const Reimagine::Engine::Window::WindowFlags& rhs);
bool operator&(const Reimagine::Engine::Window::WindowFlags& lhs, const Reimagine::Engine::Window::WindowFlags& rhs);