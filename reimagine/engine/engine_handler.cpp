#include <engine/engine_handler.h>
#include <fstream>

namespace Reimagine { namespace Engine {

  EngineHandler::EngineHandler(
    const std::filesystem::path& configFilePath,
    std::shared_ptr<ReCommon::Debugger::DebuggerHandler> debuggerHandler)
    : ConfigFilePath{configFilePath}
    , DebuggerHandler{debuggerHandler}
  {
    assert(DebuggerHandler);
  }

  void EngineHandler::Start()
  {
    using System::Internal::ReimagineEngine;

    Engine = std::make_unique<System::Core>(DebuggerHandler);
    if (!Engine)
    {
      LOG(ERR) << "Unable to create engine.";
      std::exit(EXIT_FAILURE);
    }
    ReimagineEngine = Engine.get();
  }

  void EngineHandler::Initialise()
  {
    CreateSystems();
  }

  bool EngineHandler::IsRunning() const
  {
    return Engine->IsRunning();
  }

  void EngineHandler::Update()
  {
    Engine->Update();
  }

  void EngineHandler::PostUpdate()
  {
    Engine->PostUpdate();
  }

  void EngineHandler::Destroy()
  {
    Engine->Destroy();
  }

  void EngineHandler::CreateSystems()
  {
    System::Core::SystemCollection systems;

    systems[Common::to_underlying_type(System::SystemTypes::WindowSystem)] = CreateWindowSystem();
    systems[Common::to_underlying_type(System::SystemTypes::InputSystem)] = CreateInputManager();
#ifdef DEV_TEST_ACTIVE
    systems[Common::to_underlying_type(System::SystemTypes::DevelopmentTestingSystem)] = CreateTestingEnvironment();
#endif

    Engine->Initialise(std::move(systems));
  }

  std::optional<Json::Value> EngineHandler::JsonFileReader() const
  {
    std::ifstream jsonFile{ConfigFilePath.string().c_str()};
    if (!jsonFile.is_open())
    {
      LOG(ERR) << "Unable to open " << ConfigFilePath.string() << ".";
      return std::nullopt;
    }

    Json::Value root;
    jsonFile >> root;

    jsonFile.close();
    return root;
  }

  std::unique_ptr<Window::WindowSystem> EngineHandler::CreateWindowSystem()
  {
    const auto& root = JsonFileReader();
    if (!root || !root->isMember("window"))
    {
      LOG(ERR) << "Invalid json configuration - No window configuration.";
      return nullptr;
    }

    const auto& window = root.value()["window"];
    if (!window.isMember("resolution")
      || !window.isMember("settings"))
    {
      LOG(ERR) << "Invalid json configuration - Invalid window format.";
      return nullptr;
    }

    const auto& resolution = window["resolution"];
    if (!resolution.isMember("width") || !resolution.isMember("height"))
    {
      LOG(ERR) << "Invalid json configuration - Invalid resolution format.";
      return nullptr;
    }

    const auto& settings = window["settings"];
    if (!settings.isMember("render") || !settings.isMember("display"))
    {
      LOG(ERR) << "Invalid json configuration - Invalid settings format.";
      return nullptr;
    }

    const auto& width = resolution["width"];
    const auto& height = resolution["height"];
    const auto& render = settings["render"];
    const auto& display = settings["display"];

    using WindowFlags = Window::WindowFlags;
    WindowFlags flags{};
    // by default SDL is in windowed mode
    if (display == "fullscreen")
      flags = flags | WindowFlags::FULLSCREEN;
    else if (display == "borderless")
      flags = flags | WindowFlags::BORDERLESS;
    else if (display == "windowed")
      flags = flags | WindowFlags::WINDOWED;

    if (render == "OpenGL")
      flags = flags | WindowFlags::OPENGL;
    else if (render == "Vulkan")
      flags = flags | WindowFlags::VULKAN;

    return std::make_unique<Window::WindowSystem>(
      Engine.get(),
      "Reimagine Engine",
      width.asInt(),
      height.asInt(),
      flags);
  }

  std::unique_ptr<Input::InputManager> EngineHandler::CreateInputManager()
  {
    return std::make_unique<Input::InputManager>(Engine.get());
  }

#ifdef DEV_TEST_ACTIVE
  std::unique_ptr<DevTest::DevelopmentTesting> EngineHandler::CreateTestingEnvironment()
  {
    return std::make_unique<DevTest::DevelopmentTesting>(Engine.get());
  }
#endif

} }