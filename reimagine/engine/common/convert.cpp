/**
 * @file    convert.cpp
 * @brief   Convert functions
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#pragma once

#include <engine/common/convert.h>
#include <algorithm>
#include <codecvt>
#include <locale>

namespace Reimagine { namespace Engine { namespace Common {

  std::wstring ToWString(const std::string& str)
  {
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    return converter.from_bytes(str);
  }

  std::string ToString(const std::wstring& wstr)
  {
    std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> converter;
    return converter.to_bytes(wstr);
  }

  std::string ToFrontslash(std::string path)
  {
    std::transform(
      path.begin(),
      path.end(),
      path.begin(),
      [] (char elem)
      {
        return elem == '\\' ? '/' : elem;
      });
    return path;
  }

  std::string ToBackslash(std::string path)
  {
    std::transform(
      path.begin(),
      path.end(),
      path.begin(),
      [] (char elem)
      {
        return elem == '/' ? '\\' : elem;
      });
    return path;
  }

} } }