/**
 * @file    file_handler.h
 * @brief   FileHandler
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#pragma once

#include <engine/common/file_system/file_handler_interface.h>
#include <ctime>
#include <mutex>

namespace Reimagine { namespace Engine { namespace Common { namespace FileSystem {

  /**
   * @brief Append the list of given paths using filesystem
   * @tparam PathT Type of path
   * @tparam RestPathT Type of path
   * @param path The path
   * @param paths The paths
   * @return path in std::string
   */
  template <typename PathT, typename ... RestPathT>
  PathT AppendPath(PathT&& path, RestPathT&& ... paths)
  {
    auto finalPath = std::filesystem::path(std::forward<PathT>(path));
    (finalPath.append(std::forward<RestPathT>(paths)), ...);
    if constexpr (std::is_same_v(PathT, std::string))
      return finalPath.string();
    else
      return finalPath;
  }

  std::filesystem::path GetExecutableDirectory();

  class FileHandler final : public FileHandlerInterface
  {
  public:

    FileHandler(const std::filesystem::path& relativeDirectory = {});

    /**
     * @brief Deletes a file from the specified directory
     * @param filename The filename
     * @param directory The directory
     */
    bool Delete(
      const std::string& filename,
      const std::filesystem::path& directory = {}) override;

    /**
     * @brief Gets the directory.
     *        Default path: <exe location>/Reimagine/
     *        If relativeDirectory is specified during FileHandler constructor,
     *        then GetDirectory will return <exe location>/Reimagine/<relativeDirectory>/
     */
    std::filesystem::path GetDirectory() const noexcept override;

    /**
     * @brief Retrieves information about a file's last updated time in format:
     *        dd-mm-YYYY HH:MM:SS
     * @param filename The filename
     * @param directory The directory
     */
    std::optional<std::string> GetFileLastModifiedTime(
      const std::string& filename,
      const std::filesystem::path& directory = {}) override;

    /**
     * @brief Saves a file in the specified directory
     * @param filename The filename
     * @param content The content
     * @param directory The directory
     */
    bool Save(
      const std::string& filename,
      const std::string& content,
      const std::filesystem::path& directory = {}) override;

  private:
    std::time_t ToTimeT(const std::filesystem::file_time_type& timePoint);

    std::mutex Mutex;
    std::filesystem::path Directory;
  };

  extern std::filesystem::path ReimagineDirectory;
  extern std::filesystem::path ConfigDirectory;
  extern std::filesystem::path LogDirectory;

} } } }