/**
 * @file    file_handler.cpp
 * @brief   Files Handler
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#include <engine/common/convert.h>
#include <engine/common/file_system/file_handler.h>
#include <chrono>
#include <fstream>
#include <sstream>

namespace Reimagine { namespace Engine { namespace Common { namespace FileSystem {

  std::filesystem::path GetExecutableDirectory()
  {
    wchar_t exePath[MAX_PATH];
    GetModuleFileNameW(NULL, exePath, MAX_PATH);
    return std::filesystem::path{Common::ToString(exePath)}.parent_path().string();
  }

  std::filesystem::path ReimagineDirectory = GetExecutableDirectory() / "Reimagine";
  std::filesystem::path ConfigDirectory = ReimagineDirectory / "Configs";
  std::filesystem::path LogDirectory = ReimagineDirectory / "Logs";

  FileHandler::FileHandler(const std::filesystem::path& relativeDirectory /*={}*/)
  {
    Directory = ReimagineDirectory / relativeDirectory;
    if (!std::filesystem::exists(Directory))
      std::filesystem::create_directory(Directory);
  }

  bool FileHandler::Delete(
    const std::string& filename,
    const std::filesystem::path& directory /*={}*/)
  {
    std::lock_guard<std::mutex> lockGuard{Mutex};

    auto fileDirectory = directory.empty() ? Directory : directory;
    auto filePath = fileDirectory / filename;
    if (!std::filesystem::exists(filePath))
    {
      LOG(WARN) << "\'" << filename << "\'" << " does not exists in the directory.";
      return false;
    }

    std::ofstream file{filePath.string()};
    if (!file.is_open())
    {
      LOG(WARN) << "\'" << filename << "\'" << " is currently in use. Unable to delete file.";
      return false;
    }

    file.close();
    std::filesystem::remove(filePath);
    return true;
  }

  std::filesystem::path FileHandler::GetDirectory() const noexcept
  {
    return Directory;
  }

  std::optional<std::string> FileHandler::GetFileLastModifiedTime(
    const std::string& filename,
    const std::filesystem::path& directory /*={}*/)
  {
    std::lock_guard<std::mutex> lockGuard{Mutex};

    auto fileDirectory = directory.empty() ? Directory : directory;
    auto filePath = fileDirectory / filename;
    if (!std::filesystem::exists(filePath))
    {
      LOG(WARN) << "\'" << filename << "\'" << " does not exists in the directory.";
      return std::nullopt;
    }

    auto lastFileWriteTime = std::filesystem::last_write_time(filePath);
    auto time = ToTimeT(lastFileWriteTime);
    struct tm globalTime;

    // Microsoft API and Standard Library deviates in terms of function signature
#ifdef _WIN32
    if (::gmtime_s(&globalTime, &time) != 0)
#else
    if (::gtime_s(&time, &globalTime) == nullptr)
#endif
    {
      LOG(ERR) << "gmtime_s encountered an error! Unable to retrieve file last modified time.";
      return std::nullopt;
    }

    std::stringstream buffer;
    buffer << std::put_time(&globalTime, "%d-%m-%Y %H:%M:%S");
    return buffer.str();
  }

  bool FileHandler::Save(
    const std::string& filename,
    const std::string& content,
    const std::filesystem::path& directory /*={}*/)
  {
    std::lock_guard<std::mutex> lockGuard{Mutex};

    auto fileDirectory = directory.empty() ? Directory : directory;
    auto filePath = fileDirectory / filename;
    if (std::filesystem::exists(filePath))
    {
      LOG(WARN) << "\'" << filename  << "\'" << " already exists in the directory.";
      return false;
    }

    auto parentPath = filePath.parent_path();
    if (!std::filesystem::exists(parentPath))
      std::filesystem::create_directory(parentPath);

    std::ofstream outfile{filePath};
    outfile << content;
    outfile.close();

    return true;
  }

  std::time_t FileHandler::ToTimeT(const std::filesystem::file_time_type& timePoint)
  {
    auto systemTime = std::chrono::time_point_cast<std::chrono::system_clock::duration>(
      timePoint - std::filesystem::file_time_type::clock::now() + std::chrono::system_clock::now());
    return std::chrono::system_clock::to_time_t(systemTime);
  }

} } } }