#pragma once

#include <re_common/noncopyable.h>
#include <filesystem>
#include <optional>

namespace Reimagine { namespace Engine { namespace Common { namespace FileSystem {

  class FileHandlerInterface : ReCommon::Noncopyable
  {
  public:
    virtual ~FileHandlerInterface() = default;

    /**
     * @brief Deletes a file from the specified directory
     * @param filename The filename
     * @param directory The directory
     */
    virtual bool Delete(
      const std::string& filename,
      const std::filesystem::path& directory = {}) = 0;

    /**
     * @brief Gets the directory.
     *        Default path: <exe location>/Reimagine/
     *        If relativeDirectory is specified during FileHandler constructor,
     *        then GetDirectory will return <exe location>/Reimagine/<relativeDirectory>/
     */
    virtual std::filesystem::path GetDirectory() const noexcept = 0;

    /**
     * @brief Retrieves information about a file's last updated time in format:
     *        dd-mm-YYYY HH:MM:SS
     * @param filename The filename
     * @param directory The directory
     */
    virtual std::optional<std::string> GetFileLastModifiedTime(
      const std::string& filename,
      const std::filesystem::path& directory = {}) = 0;

    /**
     * @brief Saves a file in the specified directory
     * @param filename The filename
     * @param content The content
     * @param directory The directory
     */
    virtual bool Save(
      const std::string& filename,
      const std::string& content,
      const std::filesystem::path& directory = {}) = 0;
  };

} } } }
