#pragma once

#include <functional>
#include <tuple>
#include <type_traits>
#include <utility>

namespace Reimagine { namespace Engine { namespace Common {

  struct invalid_class
  {
  };

  template <typename SignatureT>
  struct signature_trait : signature_trait<decltype(&SignatureT::operator())>
  {
  };

  template <typename ReturnT, typename ... ArgsT>
  struct signature_trait<ReturnT(ArgsT...)>
  {
    using func_type = std::function<ReturnT(ArgsT...)>;
    using return_type = ReturnT;
    using class_type = invalid_class;
    using args_type = std::tuple<ArgsT...>;
    using args_index_type = std::make_index_sequence<sizeof...(ArgsT)>;
  };

  template <typename ReturnT, typename ... ArgsT>
  struct signature_trait<ReturnT(*)(ArgsT...)>
  {
    using func_type = std::function<ReturnT(ArgsT...)>;
    using return_type = ReturnT;
    using class_type = invalid_class;
    using args_type = std::tuple<ArgsT...>;
    using args_index_type = std::make_index_sequence<sizeof...(ArgsT)>;
  };

  template <typename ReturnT, typename ClassT, typename ... ArgsT>
  struct signature_trait<ReturnT(ClassT::*)(ArgsT...)>
  {
    using func_type = std::function<ReturnT(ArgsT...)>;
    using return_type = ReturnT;
    using class_type = ClassT;
    using args_type = std::tuple<ArgsT...>;
    using args_index_type = std::make_index_sequence<sizeof...(ArgsT)>;
  };

  template <typename ReturnT, typename ClassT, typename ... ArgsT>
  struct signature_trait<ReturnT(ClassT::*)(ArgsT...) const>
  {
    using func_type = std::function<ReturnT(ArgsT...)>;
    using return_type = ReturnT;
    using class_type = ClassT;
    using args_type = std::tuple<ArgsT...>;
    using args_index_type = std::make_index_sequence<sizeof...(ArgsT)>;
  };

  template <typename SignatureT>
  using signature_trait_t = typename signature_trait<SignatureT>::func_type;

  template <typename SignatureT1, typename SignatureT2>
  using is_same_signature = std::is_same<
    signature_trait_t<SignatureT1>,
    signature_trait_t<SignatureT2>>;

  template <typename SignatureT1, typename SignatureT2>
  inline constexpr bool is_same_signature_v = 
    is_same_signature<SignatureT1, SignatureT2>::value;

  template <typename Enum>
  constexpr std::underlying_type_t<Enum> to_underlying_type(Enum e)
  {
    return static_cast<std::underlying_type_t<Enum>>(e);
  }

  template <typename ClassT>
  struct member_function;

  template <typename ClassT, typename ReturnT, typename ... Args>
  struct member_function<ReturnT(ClassT::*)(Args...)>
  {
    template <ReturnT(ClassT::*Func)(Args...)>
    static ReturnT adapter(ClassT& object, Args&& ... args)
    {
      return (object.*Func)(std::forward<Args>(args)...);
    }
  };

  template <class ClassT, class ReturnT, class... Args>
  struct member_function<ReturnT(ClassT::*)(Args...) const>
  {
    template <ReturnT(ClassT::*Func)(Args...) const>
    static ReturnT adapter(const ClassT &object, Args&&... args)
    {
      return (object.*Func)(std::forward<Args>(args)...);
    }
  };

} } }