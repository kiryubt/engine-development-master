/**
 * @file    dialogue.cpp
 * @brief
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#include <engine/common/convert.h>
#include <engine/common/dialogue/dialogue.h>
#include <engine/common/dialogue/stack_walker.h>
#include <engine/common/file_system/file_handler.h>
#include <engine/window/glfw_window.h>
#include <exception>
#include <windows.h>

namespace Reimagine { namespace Engine { namespace Common { namespace Dialogue {

  namespace
  {
    class ReimagineStackWalker : public StackWalker
    {
    public:
      std::string GetBuffer() const { return Buffer; }
      void ClearBuffer() { Buffer.clear(); }

    protected:
      virtual void OnOutput(LPCSTR text) override
      {
        Buffer += text;
        StackWalker::OnOutput(text);
      }

    private:
      std::string Buffer;
    };

    std::string GetCurrentTime()
    {
      auto currentTime = std::chrono::system_clock::now();
      auto toTime = std::chrono::system_clock::to_time_t(currentTime);
      std::stringstream stringStream;
      stringStream << std::put_time(std::localtime(&toTime), "%d-%m-%Y-%H-%M-%S");
      return stringStream.str();
    }
  }

  void Assert(bool condition, const std::string& message)
  {
    if (condition)
      return;

    ReimagineStackWalker stackWalker;
    stackWalker.ShowCallstack();
    auto stackMessage = stackWalker.GetBuffer();
    stackMessage += '\n' + message;

    std::string filename{"assert-"};
    filename += GetCurrentTime() + ".log";
    auto filesHandler = Common::FileSystem::FileHandler{};
    filesHandler.Save(filename, stackMessage, Common::FileSystem::LogDirectory);

    auto windowHandler = Ipc::GetWindow();
    if (!windowHandler)
    {
      LOG(WARN) << "Unable to show assertion dialogue box.";
      std::exit(EXIT_FAILURE);
    }

    auto messageBoxId = MessageBox(
      NULL,
      stackMessage.c_str(),
      "ASSERTION ERROR",
      MB_ICONSTOP | MB_RETRYCANCEL | MB_DEFBUTTON1);

    switch (messageBoxId)
    {
    case IDRETRY:
      throw std::runtime_error{message.c_str()};
      break;
    case IDCANCEL:
      // premature termination
      break;
    }
    std::exit(EXIT_FAILURE);
  }

  void ErrorMessage(const std::string& message)
  {
    auto windowHandler = Ipc::GetWindow();
    if (!windowHandler)
    {
      LOG(WARN) << "Unable to show error dialogue box.";
      return;
    }

    MessageBox(
      NULL,
      message.c_str(),
      NULL,
      MB_ICONEXCLAMATION);
  }

  void InfoMessage(const std::string& message)
  {
    auto windowHandler = Ipc::GetWindow();
    if (!windowHandler)
    {
      LOG(WARN) << "Unable to show information dialogue box.";
      return;
    }

    MessageBox(
      NULL,
      message.c_str(),
      "INFO",
      MB_OK | MB_ICONINFORMATION);
  }

  void ConfirmationMessage(
    const std::string& message,
    MessageCallbackT&& yesCallback,
    MessageCallbackT&& noCallback,
    MessageCallbackT&& cancelCallback
  )
  {
    auto windowHandler = Ipc::GetWindow();
    if (!windowHandler)
    {
      LOG(WARN) << "Unable to show confirmation dialogue box.";
      return;
    }

    auto messageBoxId = MessageBox(
      NULL,
      message.c_str(),
      NULL,
      MB_YESNOCANCEL | MB_ICONINFORMATION);

    switch (messageBoxId)
    {
    case IDYES:
      if (yesCallback)
        yesCallback();
      break;
    case IDNO:
      if (noCallback)
        noCallback();
      break;
    case IDCANCEL:
      if (cancelCallback)
        cancelCallback();
      break;
    }
  }

  std::optional<bool> ConfirmationMessage(const std::string& message)
  {
    auto windowHandler = Ipc::GetWindow();
    if (!windowHandler)
    {
      LOG(WARN) << "Unable to show confirmation dialogue box.";
      return std::nullopt;
    }

    auto messageBoxId = MessageBox(
      NULL,
      message.c_str(),
      NULL,
      MB_YESNOCANCEL | MB_ICONINFORMATION);

    switch (messageBoxId)
    {
    case IDYES:
      return true;
    case IDNO:
      return false;
    case IDCANCEL:
      // nullopt
      break;
    }
    return std::nullopt;
  }

} } } }