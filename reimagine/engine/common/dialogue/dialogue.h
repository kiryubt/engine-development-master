/**
 * @file    assertion.h
 * @brief   assert wrapper
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#pragma once

#include <functional>
#include <optional>

#define ASSERT(condition, message) \
  Engine::Common::Dialogue::Assert(condition, message)

namespace Reimagine { namespace Engine { namespace Common { namespace Dialogue {

  using MessageCallbackT = std::function<void()>;

  /**
   * @brief Generates an assertion with a dialogue box
   * @param condition The condition
   * @param message The message
   */
  void Assert(bool condition, const std::string& message);

  /**
   * @brief Generates an error message with a dialogue box
   * @param message The message
   */
  void ErrorMessage(const std::string& message);

  /**
   * @brief Generates an info message with a dialogue box
   * @param message The message
   */
  void InfoMessage(const std::string& message);

  /**
   * @brief Generates a confirmation message with dialogue box
   * @param message The message
   * @param yesCallback The callback for yes option
   * @param noCallback The callback for no option
   * @param cancelCallback The callback for cancel option
   */
  void ConfirmationMessage(
    const std::string& message,
    MessageCallbackT&& yesCallback,
    MessageCallbackT&& noCallback,
    MessageCallbackT&& cancelCallback);

  /**
   * @brief Generates a confirmation message with dialogue box
   * @param message The message
   */
  std::optional<bool> ConfirmationMessage(const std::string& message);

} } } }