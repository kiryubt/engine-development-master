/**
 * @file    convert.h
 * @brief   Convert functions
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#pragma once

#include <windows.h>

namespace Reimagine { namespace Engine { namespace Common {

  /**
   * @brief Converts std::string to std::wstring
   * @param str The string
   */
  std::wstring ToWString(const std::string& str);

  /**
   * @brief Converts std::wstring to std::string
   * @param wstr The wide string
   */
  std::string ToString(const std::wstring& wstr);

  /**
   * @brief Replaces all instances of Backslashes to Frontslashes
   * @param path The path
   */
  std::string ToFrontslash(std::string path);

  /**
   * @brief Replaces all instances of Frontslashes to Backslashes
   * @param path The path
   */
  std::string ToBackslash(std::string path);

} } }