#include <builder/builder.h>
#include <engine/common/file_system/file_handler.h>
#include <re_common/debugger/frame_rate/frame_rate_handler.h>
#include <re_common/debugger/timer/timer_handler.h>
#include <fstream>

namespace Reimagine { namespace Builder {

  void Builder::Initialise()
  {
    ConfigFilePath = Engine::Common::FileSystem::ConfigDirectory / "config.json";
    EnsureConfigFileExists();

    auto& logger = ReCommon::Logger::Logger::GetInstance();
    logger.Initialise(Engine::Common::FileSystem::ReimagineDirectory / "Logs");

    auto frameRateHandler = CreateFrameRateHandler();
    if (!frameRateHandler)
    {
      LOG(ERR) << "Unable to initialise frame rate handler.";
      throw std::runtime_error{"No Frame Rate Handler"};
    }

    Debugger = std::make_unique<ReCommon::Debugger::DebuggerHandler>(std::move(frameRateHandler));
    Engine = std::make_unique<Engine::EngineHandler>(ConfigFilePath, Debugger);
  }

  void Builder::Start()
  {
    assert(Debugger);
    Debugger->Start();
    Engine->Start();
  }

  void Builder::Run()
  {
    // Initialise Stage
    Engine->Initialise();

    // Update + Post Update Stage
    while (Engine->IsRunning())
    {
      Debugger->CaptureStartFrame();
      Engine->Update();
      Engine->PostUpdate();
      Debugger->CaptureEndFrame();
    }

    // Destroy Stage
    Engine->Destroy();
  }

  bool Builder::EnsureConfigFileExists() const noexcept
  {
    auto exists = true;
    if (!std::filesystem::exists(ConfigFilePath))
    {
      exists = false;
      if (!std::filesystem::exists(Engine::Common::FileSystem::ReimagineDirectory))
        std::filesystem::create_directory(Engine::Common::FileSystem::ReimagineDirectory);

      auto configLocation = ConfigFilePath.parent_path();
      if (!std::filesystem::exists(configLocation))
        std::filesystem::create_directory(configLocation);

      std::filesystem::copy_file(
        Engine::Common::FileSystem::GetExecutableDirectory() / "default" / "config.json",
        ConfigFilePath);
    }
    return exists;
  }

  std::optional<Json::Value> Builder::JsonFileReader() const
  {
    std::ifstream jsonFile{ConfigFilePath.string().c_str()};
    if (!jsonFile.is_open())
      return std::nullopt;

    Json::Value root;
    jsonFile >> root;

    jsonFile.close();
    return root;
  }

  std::unique_ptr<ReCommon::Debugger::FrameRate::FrameRateHandlerInterface> Builder::CreateFrameRateHandler() const
  {
    const auto& root = JsonFileReader();
    if (!root || !root->isMember("fps"))
    {
      LOG(ERR) << "Invalid json configuration - No fps configuration.";
      return nullptr;
    }

    const auto& fps = root.value()["fps"];
    if (!fps.isMember("limit") || !fps.isMember("value"))
    {
      LOG(ERR) << "Invalid json configuration - Invalid fps format";
      return nullptr;
    }

    const auto& limit = fps["limit"];
    const auto& value = fps["value"];

    return std::make_unique<ReCommon::Debugger::FrameRate::FrameRateHandler>(
      limit.asBool(),
      value.asUInt());
  }

} }