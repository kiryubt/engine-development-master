#pragma once

#include <engine/engine_handler.h>
#include <re_common/debugger/debugger_handler.h>
#include <re_common/debugger/frame_rate/frame_rate_interface.h>
#include <json/json.h>

namespace Reimagine { namespace Builder {

  class Builder final
  {
  public:

    /**
     * @brief Initialise components
     */
    void Initialise();

    /**
     * @brief Start Reimagine
     */
    void Start();

    /**
     * @brief Run reimagine
     */
    void Run();

  private:
    /**
     * @brief Ensure that an existing configuration file exist
     */
    bool EnsureConfigFileExists() const noexcept;

    /**
     * @brief Read the json configuration
     */
    std::optional<Json::Value> JsonFileReader() const;

    /**
     * @brief Create a frame rate handler
     */
    std::unique_ptr<ReCommon::Debugger::FrameRate::FrameRateHandlerInterface> CreateFrameRateHandler() const;

    std::unique_ptr<Engine::EngineHandler> Engine;
    std::shared_ptr<ReCommon::Debugger::DebuggerHandler> Debugger;
    std::filesystem::path ConfigFilePath;
  };

} }