/**
 * @file    logger.h
 * @brief   Logger wrapper
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#pragma once

#include <re_common/logger/spdlog_wrapper.h>
#include <re_common/noncopyable.h>
#include <sstream>
#include <filesystem>

inline constexpr auto TRACE = spdlog::level::trace;
inline constexpr auto DEBUG = spdlog::level::debug;
inline constexpr auto INFO = spdlog::level::info;
inline constexpr auto WARN = spdlog::level::warn;
inline constexpr auto ERR = spdlog::level::err;
inline constexpr auto CRITICAL = spdlog::level::critical;

//@
// Internal resolve for macro "overloading"
#define DEFAULT_LOG(SEVERITY) \
  Reimagine::ReCommon::Logger::LoggerInternal(SEVERITY, "engine")
#define COMPONENT_LOG(SEVERITY, name) \
  Reimagine::ReCommon::Logger::LoggerInternal(SEVERITY, #name)
#define USE_LOG(vardiacArgs, WRONG_LOG_VERSION, LOG_VERSION, ...) LOG_VERSION
//@

#ifdef _WIN32
// Bug in MSVC: __VA_ARGS__ counted as single argument
// Note https://stackoverflow.com/questions/5134523/msvc-doesnt-expand-va-args-correctly
#define EXPAND(vardiac) vardiac
#define LOG(...) \
  EXPAND(USE_LOG(__VA_ARGS__, COMPONENT_LOG, DEFAULT_LOG) (__VA_ARGS__))
#else
#define LOG(...) \
  USE_LOG(__VA_ARGS__, COMPONENT_LOG, DEFAULT_LOG) (__VA_ARGS__)
#endif


namespace Reimagine { namespace ReCommon { namespace Logger {

  class LoggerInternal;

  /**
   * @brief Logger class
   */
  class Logger final : Noncopyable
  {
  public:

    /**
     * @brief Get the instance of the logger
     */
    static Logger& GetInstance();

    /**
     * @brief Destroy the instance of the logger
     */
    static void DestroyInstance() noexcept;

    /**
     * @brief Initialise logger
     * @param fileLocation The file location
     */
    void Initialise(const std::filesystem::path& fileLocation);

    /**
     * @brief Logging message
     * @tparam MessageT Type of message
     * @param message The message
     */
    template <typename MessageT>
    void LogMessage(MessageT&& message, std::shared_ptr<spdlog::logger> logger)
    {
      switch(SeverityLevel)
      {
      case spdlog::level::trace:
        logger->trace(std::forward<MessageT>(message));
        break;
      
      case spdlog::level::debug:
        logger->debug(std::forward<MessageT>(message));
        break;
        
      case spdlog::level::info:
        logger->info(std::forward<MessageT>(message));
        break;

      case spdlog::level::warn:
        logger->warn(std::forward<MessageT>(message));
        break;

      case spdlog::level::err:
        logger->error(std::forward<MessageT>(message));
        break;

      case spdlog::level::critical:
        logger->critical(std::forward<MessageT>(message));
        break;
      }
    }

  private:

    static std::unique_ptr<Logger> Log;
    std::array<spdlog::sink_ptr, 2> Sinks;
    spdlog::level::level_enum SeverityLevel;

    template <typename MessageT>
    friend Reimagine::ReCommon::Logger::Logger& operator<<(Reimagine::ReCommon::Logger::Logger& log, MessageT&& message);

    friend std::unique_ptr<Logger> std::make_unique<Logger>();
    friend std::default_delete<Logger>;
    friend class LoggerInternal;
  };


  /**
   * @brief Internal class for Logger
   */
  class LoggerInternal final : Noncopyable
  {
  public:
    LoggerInternal(spdlog::level::level_enum severity, const std::string& loggerName)
    {
      auto& logger = Logger::GetInstance();
      CombinedLogger = std::make_shared<spdlog::logger>(
        loggerName,
        std::begin(logger.Sinks),
        std::end(logger.Sinks));
      logger.SeverityLevel = severity;
      CombinedLogger->set_level(DEBUG);
    }

    ~LoggerInternal()
    {
      Logger::GetInstance().LogMessage(StringStream.str(), CombinedLogger);
    }

    template <typename MessageT>
    void AppendMessage(MessageT&& message)
    {
      StringStream << std::forward<MessageT>(message);
    }

  private:
    std::stringstream StringStream;
    std::shared_ptr<spdlog::logger> CombinedLogger;
  };

} } }

template <typename MessageT>
Reimagine::ReCommon::Logger::LoggerInternal&& operator<<(
  Reimagine::ReCommon::Logger::LoggerInternal&& logger,
  MessageT&& message)
{
  logger.AppendMessage(std::forward<MessageT>(message));
  return std::move(logger);
}