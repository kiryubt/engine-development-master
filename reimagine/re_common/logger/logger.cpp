/**
 * @file    logger.cpp
 * @brief   Implementation of logger wrapper
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#include <re_common/logger/logger.h>
#include <spdlog/sinks/rotating_file_sink.h>
#include <iomanip>

namespace Reimagine { namespace ReCommon { namespace Logger {

  namespace
  {
    std::string GetCurrentTime()
    {
      namespace chrono = std::chrono;
      auto currentTime = chrono::system_clock::now();
      auto toTime = chrono::system_clock::to_time_t(currentTime);
      auto milliseconds =
        chrono::duration_cast<chrono::milliseconds>(currentTime.time_since_epoch()) % chrono::seconds(1);
      std::stringstream stringStream;
      stringStream << std::put_time(std::localtime(&toTime), "%d%m%Y_%H%M%S");
      stringStream << milliseconds.count();
      return stringStream.str();
    }

    struct FileSinkHelper final
    {
      FileSinkHelper(const std::string& logMessagePattern, const std::filesystem::path& fileLocation)
      {
        const auto& filename = std::string{"reimagine_"} + GetCurrentTime() + ".log";
        const auto& filePath = std::filesystem::path{fileLocation} / filename;
        FileSink = std::make_shared<spdlog::sinks::rotating_file_sink_mt>(
          filePath.string(),
          1048576 * 5,    // Max of 5MB
          10);            // Max of 10 files before rotating
        FileSink->set_pattern(logMessagePattern);
      }

      operator spdlog::sink_ptr() const { return FileSink; }

    private:
      spdlog::sink_ptr FileSink;
    };

    struct ConsoleSinkHelper final
    {
      ConsoleSinkHelper(const std::string& logMessagePattern)
        : TheConsoleSink{std::make_shared<ConsoleSinkMt>()}
      {
        TheConsoleSink->set_pattern(logMessagePattern);
      }

      operator spdlog::sink_ptr() const { return TheConsoleSink; }

    private:
      spdlog::sink_ptr TheConsoleSink;
    };
  }

  std::unique_ptr<Logger> Logger::Log = nullptr;

  void Logger::Initialise(const std::filesystem::path& fileLocation)
  {
    auto logMessagePattern = "[%d%m%Y %H:%M:%S.%e] [%^%L%$] [%n] %v";
    Sinks[0] = ConsoleSinkHelper{logMessagePattern};
    Sinks[1] = FileSinkHelper{logMessagePattern, fileLocation};
    spdlog::flush_every(std::chrono::seconds(5));
  }

  Logger& Logger::GetInstance()
  {
    if (!Log)
      Log = std::make_unique<Logger>();
    return *Log;
  }

  void Logger::DestroyInstance() noexcept
  {
    Log = nullptr;
  }

} } }