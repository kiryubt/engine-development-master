/**
 * @file    spdlog_wrapper.h
 * @brief   spdlog wrapper for reimagine engine
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#pragma once

#include <spdlog/details/null_mutex.h>
#include <spdlog/fmt/fmt.h>
#include <spdlog/sinks/base_sink.h>
#include <spdlog/spdlog.h>
#include <iostream>
#include <mutex>

/**
 * @brief For UDT Log Formating. This is a function signature, please provide body.
 *        LOG_PRINT_FORMAT()
 *        {
 *          return os << obj.i << " " << obj.j;
 *        }
 * @param 'this' object
 * @return void
 */
#define LOG_PRINT_FORMAT(Object) \
  friend std::ostream& operator<<(std::ostream& os, const Object& obj)

namespace Reimagine { namespace ReCommon { namespace Logger {

  template <typename MutexT>
  class ConsoleSink final : public spdlog::sinks::base_sink<MutexT>
  {
  public:
    ConsoleSink()
      : Base{}
    {
    }

  protected:
    void sink_it_(const spdlog::details::log_msg& message) override
    {
      /**
       * log_msg is a struct containing the log entry info like:
       * level
       * timestamp
       * thread id
       * etc.
       * msg.raw contains pre-formatted log
       */
      spdlog::memory_buf_t formatted{};
      Base::formatter_->format(message, formatted);
      std::cout << std::boolalpha << fmt::to_string(formatted);
    }

    void flush_() override
    {
      std::cout << std::flush;
    }

  private:
    using Base = spdlog::sinks::base_sink<MutexT>;
  };

  using ConsoleSinkMt = ConsoleSink<std::mutex>;
  using ConsoleSinkSt = ConsoleSink<spdlog::details::null_mutex>;

} } }