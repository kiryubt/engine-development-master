/**
 * @file    executor_interface.h
 * @brief   Asynchronous Executor Interface
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#pragma once

#include <re_common/noncopyable.h>
#include <taskflow/core/executor.hpp>
#include <taskflow/core/observer.hpp>
#include <taskflow/core/task.hpp>
#include <taskflow/core/taskflow.hpp>
#include <functional>
#include <optional>

namespace Reimagine { namespace ReCommon { namespace Async {

  /**
   * @brief Exectuor Interface
   */
  class ExecutorInterface : Noncopyable
  {
  public:

    using Work = std::function<void()>;

    /**
     * @brief Start the Executor
     */
    virtual void Start() = 0;

    /**
     * @brief Stop the Executor
     */
    virtual void Stop() = 0;

    /**
     * @brief Post a work
     * @param work Functor
     * @return A future object to the status of the worker
     */
    virtual std::optional<std::future<void>> Post(Work&& work) = 0;

    /**
     * @brief Visualize the current task dependency graph (if there is)
     *        Outputs to Logger
     */
    virtual void VisualizeExecutorTaskflows() const = 0;

    /**
     * @brief Get the implementation of the Executor
     */
    virtual ExecutorInterface* GetExecutorImplementation() noexcept = 0;
  };

} } }