/**
 * @file    executor.cpp
 * @brief   Asynchronous Executor
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#include <re_common/async/executor.h>
#include <re_common/logger/logger.h>

namespace Reimagine { namespace ReCommon { namespace Async {

  Executor::Executor()
    : ExecutorObserver{TheExecutor.make_observer<tf::ExecutorObserver>()}
    , TaskCounter{0}
  {
  }

  void Executor::Start()
  {
    LOG(INFO) << "Executor Started.";
  }

  void Executor::Stop()
  {
    TheExecutor.wait_for_all();
    for (auto& taskflow : Taskflows)
      taskflow.clear();
    LOG(INFO) << "Executor Stopped.";
  }

  std::optional<std::future<void>> Executor::Post(Work&& work)
  {
    Taskflows.emplace_back();
    auto& taskflow = Taskflows.back();
    auto task = taskflow.emplace(work).name(TaskName + std::to_string(TaskCounter++));
    if (task.empty())
    {
      LOG(ERR) << "Unable to Post work!";
      return std::nullopt;
    }

    return TheExecutor.run(taskflow);
  }

  void Executor::VisualizeExecutorTaskflows() const
  {
    for (const auto& taskflow : Taskflows)
    {
      if (taskflow.empty())
        continue;
      auto flow = std::move(taskflow.dump());
      LOG(INFO) << flow;
    }
  }

  Executor* Executor::GetExecutorImplementation() noexcept
  {
    return this;
  }

} } }