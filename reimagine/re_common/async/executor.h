/**
 * @file    executor.h
 * @brief   Asynchronous Executor
 * @author  Kenneth Toh (ktwgkiryu@outlook.sg)
 */

#pragma once

#include <re_common/async/executor_interface.h>
#include <list>
#include <optional>

namespace Reimagine { namespace ReCommon { namespace Async {

  /**
   * @brief Executor class
   */
  class Executor final : public ExecutorInterface
  {
  public:
    Executor();

    /**
     * @brief Start the Executor
     */
    void Start() override;

    /**
     * @brief Stop the Executor
     */
    void Stop() override;

    /**
     * @brief Post a work
     * @param work Functor
     * @return A future object to the status of the worker
     */
    std::optional<std::future<void>> Post(Work&& work) override;

    /**
     * @brief Post a chain of works
     * @tparam WorkTs Vardiac Work Type
     * @param works Chain of works
     * @return A future object to the status of the worker
     */
    template<
      typename ... WorkT,
      std::enable_if_t<(sizeof...(WorkT) > 1), char> = 0>
    std::optional<std::future<void>> Post(WorkT&& ... works)
    {
      uint16_t vardiacTaskCounter = 0;
      Taskflows.emplace_back();

      auto cascadingTasks = PostHelper(vardiacTaskCounter, works...);
      if (!cascadingTasks)
      {
        LOG(ERR) << "Unable to Post work!";
        auto& taskflow = Taskflows.back();
        taskflow.clear();
        Taskflows.pop_back();
        return nullopt;
      }

      auto& taskflow = Taskflows.back();
      return TheExecutor.run(taskflow);
    }

    /**
     * @brief Visualize the current task dependency graph (if there is)
     *        Outputs to Logger
     */
    void VisualizeExecutorTaskflows() const override;

    /**
     * @brief Get the implementation of the Executor
     */
    Executor* GetExecutorImplementation() noexcept override;

  private:

    template <typename ... WorkT>
    std::optional<tf::Task> PostHelper(
      uint16_t vardiacTaskCounter,
      Work&& work,
      WorkT&& ... otherWorks)
    {
      auto& taskflow = Taskflows.back();
      auto task = taskflow.emplace(work).name(TaskName + std::to_string(vardiacTaskCounter++));
      if (task.empty())
        return std::nullopt;

      if constexpr (sizeof...(WorkT) == 0)
      {
        return task;
      }
      else
      {
        auto nextTask = PostHelper(vardiacTaskCounter, otherWorks...);
        if (!nextTask)
          return std::nullopt;

        nextTask->succeed(task);
        return nextTask;
      }
    }

    const std::string TaskName{"ExecutorTask-"};
    tf::Executor TheExecutor;
    std::list<tf::Taskflow> Taskflows;
    tf::ExecutorObserver* ExecutorObserver;
    uint16_t TaskCounter;
  };

} } }