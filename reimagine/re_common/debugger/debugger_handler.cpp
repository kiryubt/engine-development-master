#include <re_common/debugger/debugger_handler.h>
#include <re_common/debugger/timer/timer_handler.h>
#include <re_common/logger/logger.h>

namespace Reimagine { namespace ReCommon { namespace Debugger {

  void RecordStart(const std::optional<std::string>& name) noexcept
  {
    auto& timerHandler = DebuggerHandler::GetTimerHandler();

    if (name.has_value())
      timerHandler.NamedRecordStart(name.value());
    else
      timerHandler.RecordStart();
  }

  std::optional<std::chrono::milliseconds> RecordEnd(const std::optional<std::string>& name) noexcept
  {
    auto& timerHandler = DebuggerHandler::GetTimerHandler();

    if (name.has_value())
    {
      timerHandler.NamedRecordEnd(name.value());
      return timerHandler.GetNamedRecordTimepoint(name.value());
    }
    
    return timerHandler.RecordEnd();
  }

  std::optional<std::chrono::milliseconds> GetNamedRecordedTime(const std::string& name) noexcept
  {
    auto& timerHandler = DebuggerHandler::GetTimerHandler();

    return timerHandler.GetNamedRecordTimepoint(name);
  }

  std::unique_ptr<Timer::TimerHandlerInterface> DebuggerHandler::TimerHandler = nullptr;

  DebuggerHandler::DebuggerHandler(
      std::unique_ptr<FrameRate::FrameRateHandlerInterface> frameRateHandler)
    : FrameRateHandler{std::move(frameRateHandler)}
  {
    assert(FrameRateHandler);
  }

  void DebuggerHandler::CaptureStartFrame() noexcept
  {
    FrameRateHandler->StartCapture();
  }

  void DebuggerHandler::CaptureEndFrame() noexcept
  {
    FrameRateHandler->EndCapture();
  }

  void DebuggerHandler::Start()
  {
    GetTimerHandler().StartElapsedTime();
  }

  const FrameRate::FrameRateHandlerInterface& DebuggerHandler::GetFrameRateHandler() const noexcept
  {
    return *FrameRateHandler;
  }

  void DebuggerHandler::SetFPSLimit(bool toLimit, uint8_t framesLimit /*= 60*/) noexcept
  {
    FrameRateHandler->LimitFrameRate(toLimit, framesLimit);
  }

  std::optional<std::chrono::milliseconds> DebuggerHandler::GetElapsedTime() const noexcept
  {
    return TimerHandler->GetElapsedTime();
  }

  Timer::TimerHandlerInterface& DebuggerHandler::GetTimerHandler() noexcept
  {
    if (!TimerHandler)
      TimerHandler = std::make_unique<Timer::TimerHandler>();
    return *TimerHandler.get();
  }

} } }