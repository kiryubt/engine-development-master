#include <re_common/debugger/frame_rate/frame_rate_handler.h>
#include <cassert>
#include <stdexcept>
#include <limits>

namespace Reimagine { namespace ReCommon { namespace Debugger { namespace FrameRate {

  namespace
  {
    static constexpr auto Epsilon = std::numeric_limits<float>::epsilon();
  }

  FrameRateHandler::FrameRateHandler(bool limit /*= true*/, uint8_t limitValue /*= 60*/)
    : DeltaTime{0}
    , PerFrameTime{0}
    , LimitValue{limitValue}
    , Limit{limit}
  {
    // Since window is created later, it is not possible to use our engine's ASSERT
    if (!LimitValue)
      throw std::overflow_error{"Division by zero exception"};
    PerFrameTime = 1.f / static_cast<float>(LimitValue);
  }

  void FrameRateHandler::StartCapture() noexcept
  {
    TimeStart = HighResClock::now();
  }

  void FrameRateHandler::EndCapture() noexcept
  {
    auto calculateDeltaTime =
      [this] () mutable
      {
        auto timeEnd = HighResClock::now();
        auto timeSpan = std::chrono::duration_cast<std::chrono::duration<float>>(timeEnd - TimeStart);
        DeltaTime = timeSpan.count();
      };

    DeltaTime = 0;
    if (Limit)
    {
      while (DeltaTime < PerFrameTime)
        calculateDeltaTime();
    }
    else
    {
      calculateDeltaTime();
    }
  }

  float FrameRateHandler::GetDeltaTime() const noexcept
  {
    return DeltaTime;
  }

  float FrameRateHandler::GetFPS() const noexcept
  {
    if (-Epsilon <= DeltaTime && DeltaTime <= Epsilon)
      return 1.f;
    return 1.f / DeltaTime;
  }

  void FrameRateHandler::LimitFrameRate(bool toLimit, uint8_t limitValue /*=60*/) noexcept
  {
    assert(limitValue);
    Limit = toLimit;
    if (Limit)
    {
      LimitValue = limitValue;
      PerFrameTime = 1.f / static_cast<float>(LimitValue);
    }
  }

  bool FrameRateHandler::IsFrameRateCapped() const noexcept
  {
    return Limit;
  }

  uint8_t FrameRateHandler::GetFramesLimit() const noexcept
  {
    return LimitValue;
  }

} } } }