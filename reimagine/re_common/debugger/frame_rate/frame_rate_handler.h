#pragma once

#include <re_common/debugger/frame_rate/frame_rate_interface.h>
#include <atomic>
#include <chrono>

namespace Reimagine { namespace ReCommon { namespace Debugger { namespace FrameRate {

  class FrameRateHandler final : public FrameRateHandlerInterface
  {
  public:
    FrameRateHandler(bool limit = true, uint8_t limitValue = 60);

    /**
     * @brief Start capturing time for frame rate
     */
    void StartCapture() noexcept override;

    /**
     * @brief End capturing time for frame rate
     */
    void EndCapture() noexcept override;

    /**
     * @brief Retrieves the delta time (from the previous iteration)
     */
    float GetDeltaTime() const noexcept override;

    /**
     * @brief Retrieves the FPS
     */
    float GetFPS() const noexcept override;

    /**
     * @brief Limits or unlimits the frame rate
     * @param toLimit The toLimit
     * @param framesLimit The frameLimit
     */
    void LimitFrameRate(bool toLimit, uint8_t framesLimit = 60) noexcept override;

    /**
     * @brief Determines if the frame rate is capped
     */
    bool IsFrameRateCapped() const noexcept override;

    /**
     * @brief Retrieves the frame limit
     */
    virtual uint8_t GetFramesLimit() const noexcept override;

  private:
    using HighResClock = std::chrono::high_resolution_clock;

    HighResClock::time_point TimeStart;
    float DeltaTime;
    float PerFrameTime;
    uint8_t LimitValue;
    std::atomic<bool> Limit;
  };

} } } }