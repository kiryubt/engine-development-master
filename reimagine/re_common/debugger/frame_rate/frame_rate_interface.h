#pragma once

#include <re_common/noncopyable.h>
#include <cstdint>

namespace Reimagine { namespace ReCommon { namespace Debugger { namespace FrameRate {

  class FrameRateHandlerInterface : Noncopyable
  {
  public:

    /**
     * @brief Start capturing time for frame rate
     */
    virtual void StartCapture() noexcept = 0;

    /**
     * @brief End capturing time for frame rate
     */
    virtual void EndCapture() noexcept = 0;

    /**
     * @brief Retrieves the delta time (from the previous iteration)
     */
    virtual float GetDeltaTime() const noexcept = 0;

    /**
     * @brief Retrieves the FPS
     */
    virtual float GetFPS() const noexcept = 0;

    /**
     * @brief Limits or unlimits the frame rate
     * @param toLimit The toLimit
     * @param framesLimit The frameLimit
     */
    virtual void LimitFrameRate(bool toLimit, uint8_t framesLimit = 60) noexcept = 0;

    /**
     * @brief Determines if the frame rate is capped
     */
    virtual bool IsFrameRateCapped() const noexcept= 0;

    /**
     * @brief Retrieves the frame limit
     */
    virtual uint8_t GetFramesLimit() const noexcept = 0;
  };

} } } }