#pragma once

#include <re_common/debugger/timer/timer_handler_interface.h>
#include <re_common/debugger/frame_rate/frame_rate_interface.h>
#include <memory>

namespace Reimagine { namespace ReCommon { namespace Debugger {

  /**
   * @brief Begin recording current time
   * @param name [opt] recorded name
   */
  void RecordStart(const std::optional<std::string>& name = std::nullopt) noexcept;

  /**
   * @brief End recording current time and return results
   * @param name [opt] recorded name
   */
  std::optional<std::chrono::milliseconds> RecordEnd(const std::optional<std::string>& name = std::nullopt) noexcept;

  /**
   * @brief Returns the recorded name of the given name
   * @param name recorded name
   */
  std::optional<std::chrono::milliseconds> GetNamedRecordedTime(const std::string& name) noexcept;

  /**
   * @brief Debugger wrapper for project
   *        This should be constructed in Builder
   */
  class DebuggerHandler final : Noncopyable
  {
  public:

    DebuggerHandler(
      std::unique_ptr<FrameRate::FrameRateHandlerInterface> frameRateHandler);

    /**
     * @brief Start the debugger
     */
    void Start();

    /**
     * @brief Capture the start frame
     */
    void CaptureStartFrame() noexcept;

    /**
     * @brief Capture the end frame and calculate delta time
     */
    void CaptureEndFrame() noexcept;

    /**
     * @brief Retrieves the frame rate handler
     */
    const FrameRate::FrameRateHandlerInterface& GetFrameRateHandler() const noexcept;

    /**
     * @brief Limits or unlimits the frame rate
     * @param toLimit The toLimit
     * @param framesLimit The frameLimit
     */
    void SetFPSLimit(bool toLimit, uint8_t framesLimit = 60) noexcept;

    /**
     * @brief Get elapsed time for the engine
     */
    std::optional<std::chrono::milliseconds> GetElapsedTime() const noexcept;

    /**
     * @brief Retrieves the timer handler
     */
    static Timer::TimerHandlerInterface& GetTimerHandler() noexcept;

  private:
    static std::unique_ptr<Timer::TimerHandlerInterface> TimerHandler;
    std::unique_ptr<FrameRate::FrameRateHandlerInterface> FrameRateHandler;
  };

} } }