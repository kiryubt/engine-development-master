#pragma once

#include <re_common/noncopyable.h>
#include <chrono>
#include <optional>

namespace Reimagine { namespace ReCommon { namespace Debugger { namespace Timer {

  /**
   * @brief Timer Interface
   */
  class TimerHandlerInterface : Noncopyable
  {
  public:

    using ClockType = std::chrono::high_resolution_clock;
    using SystemClockType = std::chrono::system_clock;

    /**
     * @brief Start recording the current time
     */
    virtual void RecordStart() noexcept = 0;

    /**
     * @brief Calculates the time point between RecordStart and RecordEnd
     *        If RecordStart is not called before, this function does nothing
     * @return time in milliseconds if valid, else boost::none
     */
    virtual std::optional<std::chrono::milliseconds> RecordEnd() noexcept = 0;

    /**
     * @brief Start recording the current time and store into a container with recorded name
     * @param name recorded name
     */
    virtual void NamedRecordStart(const std::string& name) noexcept = 0;

    /**
     * @brief Stores the time taken between NamedRecordStart and this function
     * @param name recorded name
     */
    virtual void NamedRecordEnd(const std::string& name) noexcept = 0;

    /**
     * @brief Begin recording elapsed time
     */
    virtual void StartElapsedTime() noexcept = 0;

    /**
     * @brief Get elapsed time
     *        If StartElapsedTime is not called before, this function does nothing
     * @return time in milliseconds if valid, else boost::none
     */
    virtual std::optional<std::chrono::milliseconds> GetElapsedTime() noexcept = 0;

    /**
     * @brief Clear all recorded timepoints. Elapsed Time should not be cleared
     */
    virtual void ClearRecordedTimes() noexcept = 0;

    /**
     * @brief Retrieves the time duration for the recorded name timepoint
     *        Returns nullopt if name is not found or a valid time is not successfully recorded
     * @param name recorded name
     */
    virtual std::optional<std::chrono::milliseconds> GetNamedRecordTimepoint(const std::string& name) noexcept = 0;
  };

} } } }