#include <re_common/debugger/timer/timer_handler.h>
#include <re_common/logger/logger.h>

namespace Reimagine { namespace ReCommon { namespace Debugger { namespace Timer {

  void TimerHandler::RecordStart() noexcept
  {
    std::lock_guard<std::mutex> lockGuard{Mutex};
    RecordTimepoints.emplace(ClockType::now());
  }

  std::optional<std::chrono::milliseconds> TimerHandler::RecordEnd() noexcept
  {
    std::lock_guard<std::mutex> lockGuard{Mutex};
    if (RecordTimepoints.empty())
      return std::nullopt;

    const auto& startTime = RecordTimepoints.top();
    const auto& currentTime = ClockType::now();
    const auto& duration = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - startTime);
    RecordTimepoints.pop();
    return duration;
  }

  void TimerHandler::NamedRecordStart(const std::string& name) noexcept
  {
    std::lock_guard<std::mutex> lockGuard{Mutex};

    auto& [timepoint, time] = NamedRecordTimepoints[name];
    timepoint = ClockType::now();
  }

  void TimerHandler::NamedRecordEnd(const std::string& name) noexcept
  {
    std::lock_guard<std::mutex> lockGuard{Mutex};
    if (NamedRecordTimepoints.find(name) == NamedRecordTimepoints.end())
    {
      LOG(WARN) << name << " does not exist in the recorded list of timer handler";
      return;
    }

    const auto& currentTime = ClockType::now();
    auto& [timepoint, time] = NamedRecordTimepoints[name];
    if (!timepoint.has_value())
    {
      LOG(WARN) << "Timepoint for " << name << " is called prematurely without NAMED_RECORD_START";
      return;
    }

    time = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - timepoint.value());
  }

  void TimerHandler::StartElapsedTime() noexcept
  {
    std::lock_guard<std::mutex> lockGuard{Mutex};
    SystemTimepoint = SystemClockType::now();
  }

  std::optional<std::chrono::milliseconds> TimerHandler::GetElapsedTime() noexcept
  {
    std::lock_guard<std::mutex> lockGuard{Mutex};
    if (!SystemTimepoint)
      return std::nullopt;

    const auto& currentTime = SystemClockType::now();
    const auto& duration = std::chrono::duration_cast<std::chrono::milliseconds>(currentTime - SystemTimepoint.value());
    return duration;
  }

  void TimerHandler::ClearRecordedTimes() noexcept
  {
    std::lock_guard<std::mutex> lockGuard{Mutex};
    TimepointsCollection defaultTimepoints{};
    RecordTimepoints.swap(defaultTimepoints);
    NamedRecordTimepoints.clear();
  }

  std::optional<std::chrono::milliseconds> TimerHandler::GetNamedRecordTimepoint(const std::string& name) noexcept
  {
    if (NamedRecordTimepoints.empty())
      return std::nullopt;

    std::lock_guard<std::mutex> lockGuard{Mutex};
    auto iter = NamedRecordTimepoints.find(name);
    if (iter == NamedRecordTimepoints.end())
    {
      LOG(WARN) << name << " is not found in recorded list of timer handler";
      return std::nullopt;
    }

    const auto& [recordedName, timePointInfo] = *iter;
    const auto& [timepoint, time] = timePointInfo;
    if (!time.has_value())
    {
      LOG(WARN) << "Time failed to record for : " << name;
      return std::nullopt;
    }

    return time;
  }

} } } }