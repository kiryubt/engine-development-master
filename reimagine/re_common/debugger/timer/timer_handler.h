#pragma once

#include <re_common/debugger/timer/timer_handler_interface.h>
#include <mutex>
#include <stack>
#include <unordered_map>

namespace Reimagine { namespace ReCommon { namespace Debugger { namespace Timer {

  /**
   * @brief Timer class
   */
  class TimerHandler final : public TimerHandlerInterface
  {
  public:

    /**
     * @brief Start recording the current time
     */
    void RecordStart() noexcept override;

    /**
     * @brief Calculates the time point between RecordStart and RecordEnd
     *        If RecordStart is not called before, this function does nothing
     * @return time in milliseconds if valid, else boost::none
     */
    std::optional<std::chrono::milliseconds> RecordEnd() noexcept override;

    /**
     * @brief Start recording the current time and store into a container with recorded name
     * @param name recorded name
     */
    void NamedRecordStart(const std::string& name) noexcept override;

    /**
     * @brief Stores the time taken between NamedRecordStart and this function
     * @param name recorded name
     */
    void NamedRecordEnd(const std::string& name) noexcept override;

    /**
     * @brief Begin recording elapsed time
     */
    void StartElapsedTime() noexcept override;

    /**
     * @brief Get elapsed time
     *        If StartElapsedTime is not called before, this function does nothing
     * @return time in milliseconds if valid, else boost::none
     */
    std::optional<std::chrono::milliseconds> GetElapsedTime() noexcept override;

    /**
     * @brief Clear all recorded timepoints. Elapsed Time should not be cleared
     */
    void ClearRecordedTimes() noexcept override;

    /**
     * @brief Retrieves the time duration for the recorded name timepoint
     *        Returns nullopt if name is not found or a valid time is not successfully recorded
     * @param name recorded name
     */
    std::optional<std::chrono::milliseconds> GetNamedRecordTimepoint(const std::string& name) noexcept override;

  private:
    using TimepointsCollection = std::stack<ClockType::time_point>;
    using NamedTimepointsCollection = std::unordered_map<
      std::string,
      std::pair<std::optional<ClockType::time_point>, std::optional<std::chrono::milliseconds>>>;

    std::mutex Mutex;
    NamedTimepointsCollection NamedRecordTimepoints;
    TimepointsCollection RecordTimepoints;
    std::optional<SystemClockType::time_point> SystemTimepoint;
  };

} } } }