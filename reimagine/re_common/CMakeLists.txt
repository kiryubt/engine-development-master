add_subdirectory(async)
add_subdirectory(debugger)
add_subdirectory(logger)

add_reimagine_library(re_common
    PCH
        logger/logger.h
        debugger/debugger_handler.h
        noncopyable.h
    INTERFACE)

link_reimagine_libraries(re_common
    INTERFACE
    LIBRARIES
        re_common_async
        re_common_logger)