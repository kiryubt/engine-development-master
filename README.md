# Requirements
- CMake Version 3.17 (at least)
- Python 3.7 (at least)
- Git
- Access to Engine-Development-Master and third-party-dependencies repositories
- Visual Studio 2019 Build Tools (or Visual Studio 2019)

# Environmental Setup
1. Ensure that Git, CMake and python and visual studio are present in environmental paths. Project runs on Visual Studio 16 2019 Win64 build
2. Ensure SSH keys from working machine is registered in bitbucket account
3. For Visual Studio 2019 installation, add in the additional components to build:  
  - C++ 2019 Redistributable Update  
  - C++ CMake tools for Windows  
  - MSBuild  
  - MSVC v142 - VS 2019 C++ x64/x86 build tools (v14.25)  
  - C++ core features  
  - C++ ATL for latest v142 build tools (x86 & x64)  
  - Windows 10 SDK (10.0.18362.0)  
  - Ensure that version of VS 2019 is 16.6.0 or higher (Visual Studio cmake version should be 3.17 or higher)  

# Installation
1. In Root Directory > Launch Command Prompt > Type in the following: ```python buildsystem/build.py```  
  - If there's issues with dependencies, perform the following:  
    1. Create new folder named ```.deps```  
    2. Clone Third-Party-Dependencies repo  
    3. Copy all contents in repo (except for .git) to engine-development-master repo > ```.deps```  
2. Wait for build script to finish building engine
3. Launch engine from ```.build.x64.<build_type>``` generated folders
4. For subsequent existing builds, its possible to use the following command: ```python buildsystem\build.py -sr```  
   to skip dependencies fetching
5. To update only dependencies folder, use: ```python buildsystem\build.py -ud```

## Breakdown Of Framework
This framework combines python script and CMake capabilities to compile, link and import any necessary libraries to build the engine.  
The order of execution is of the following:  
  
1. Python Script retrieves external dependencies for engine
2. Python Script calls CMake with arguments for building
3. CMake begins to build the engine
4. Once engine has been successfully built, Python Script will import any necessary dynamic libraries for the executable to run
5. Engine is now ready to run