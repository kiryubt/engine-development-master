function(print_CXX_MSVC_flags)
    message(STATUS "CMAKE_CXX_FLAGS_DEBUG is ${CMAKE_CXX_FLAGS_DEBUG}")
    message(STATUS "CMAKE_CXX_FLAGS_RELEASE is ${CMAKE_CXX_FLAGS_RELEASE}")
    message(STATUS "CMAKE_CXX_FLAGS_RELWITHDEBINFO is ${CMAKE_CXX_FLAGS_RELWITHDEBINFO}")
endfunction()

option(DEV_TEST_ENVIRONMENT "Development Test Environment" OFF)

function(set_project_flags project_name)

    option(WARNINGS_AS_ERRORS "Treat compiler warnings as errors" OFF)

    set(MSVC_DEBUG
        /JMC    # Supports native C++ Just My Code Debugging
    )
    
    set(MSVC_WARNINGS
        /W3     # Baseline reasonable warnings
        /w14242 # 'identfier': conversion from 'type1' to 'type1', possible loss of data
        /w14254 # 'operator': conversion from 'type1:field_bits' to 'type2:field_bits', possible loss of data
        /w14263 # 'function': member function does not override any base class virtual member function
        /w14265 # 'classname': class has virtual functions, but destructor is not virtual instances of this class may not be destructed correctly
        /w14287 # 'operator': unsigned/negative constant mismatch
        /we4289 # nonstandard extension used: 'variable': loop control variable declared in the for-loop is used outside the for-loop scope
        /w14296 # 'operator': expression is always 'boolean_value'
        /w14311 # 'variable': pointer truncation from 'type1' to 'type2'
        /w14545 # expression before comma evaluates to a function which is missing an argument list
        /w14546 # function call before comma missing argument list
        /w14547 # 'operator': operator before comma has no effect; expected operator with side-effect
        /w14549 # 'operator': operator before comma has no effect; did you intend 'operator'?
        /w14555 # expression has no effect; expected expression with side- effect
        /w14619 # pragma warning: there is no warning number 'number'
        /w14640 # Enable warning on thread un-safe static member initialization
        /w14826 # Conversion from 'type1' to 'type_2' is sign-extended. This may cause unexpected runtime behavior.
        /w14905 # wide string literal cast to 'LPSTR'
        /w14906 # string literal cast to 'LPWSTR'
        /w14928 # illegal copy-initialization; more than one user-defined conversion has been implicitly applied
    )

    set(MSVC_FLAGS
        /DGLM_FORCE_MESSAGES # Report Configuration and adjust build according to project's configurations
        /DWIN32_LEAN_AND_MEAN
        /DNOMINMAX
        $<$<BOOL:${DEV_TEST_ENVIRONMENT}>:/DDEV_TEST_ACTIVE>
        /MP)
    if(${CMAKE_BUILD_TYPE} MATCHES Debug)
        set(MSVC_FLAGS ${MSVC_FLAGS} ${MSVC_DEBUG})
    endif()

    if (WARNINGS_AS_ERRORS)
        set(MSVC_WARNINGS ${MSVC_WARNINGS} /WX)
    endif()

    set(PROJECT_FLAGS ${MSVC_WARNINGS} ${MSVC_FLAGS})
    message(STATUS ${PROJECT_FLAGS})
    target_compile_options(${project_name} INTERFACE ${PROJECT_FLAGS})

endfunction()