option(ENABLE_IPO "Enable Iterprocedural Optimization, aka Link Time Optimization (LTO)" OFF)
if (ENABLE_IPO)
    include(CheckIPOSupported)
    check_ipo_supported(RESULT result OUTPUT output)
    if (result)
        set(CMAKE_INTERPROCEDURAL_OPTIMIZATION TRUE)
    else()
        message(SEND_ERROR "IPO is not supported: ${output}")
    endif()
endif()

option(BUILD_ENGINE "Building the engine" ON)


macro(SUB_DIR_LIST result curdir)

    file(GLOB children RELATIVE ${curdir} ${curdir}/*)
    set(dirlist "")
    foreach (child ${children})
        if (IS_DIRECTORY ${curdir}/${child})
            list(APPEND dirlist ${child})
        endif()
    endforeach()
    set(${result} ${dirlist})

endmacro()


function(add_reimagine_library name)

    set(options STATIC SHARED INTERFACE)
    set(oneValueArgs )
    set(multiValueArgs SOURCES PCH)
    cmake_parse_arguments(add_reimagine_library "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    set(variant STATIC)
    if (${add_reimagine_library_INTERFACE})
        set(variant INTERFACE)
    elseif (${add_reimagine_library_SHARED})
        set(variant SHARED)
    endif()

    set(sources ${add_reimagine_library_SOURCES})

    add_library(${name}
        ${variant}
        ${sources})

    set(pch ${add_reimagine_library_PCH})
    if (ENABLE_PCH)
        target_precompile_headers(project_options INTERFACE
            ${pch})
    endif()

    if (NOT ${add_reimagine_library_INTERFACE})
        target_link_libraries(${name}
            PRIVATE
            project_options
            project_flags)
    endif()

    if (${add_reimagine_library_SHARED})
        target_compile_definitions(${name}
            PRIVATE
                REIMAGINE_DLL=1)
    endif()

endfunction()


function(link_reimagine_libraries name)

    set(options EXTERNAL SHARED INTERFACE)
    set(oneValueArgs )
    set(multiValueArgs DEBUG RELEASE LIBRARIES)
    cmake_parse_arguments(link_reimagine_libraries "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    set(variant PRIVATE)
    if (${link_reimagine_libraries_INTERFACE} OR ${link_reimagine_libraries_SHARED})
        set(variant INTERFACE)
    endif()

    target_link_libraries(${name}
        ${variant}
        ${link_reimagine_libraries_LIBRARIES})

    if (${link_reimagine_libraries_EXTERNAL})
        target_link_libraries(${name}
            ${variant}
            debug
            ${link_reimagine_libraries_DEBUG})

        target_link_libraries(${name}
            ${variant}
            optimized
            ${link_reimagine_libraries_RELEASE})

        target_link_libraries(${name}
            ${variant}
            general
            ${link_reimagine_libraries_RELEASE})
    endif()

endfunction()